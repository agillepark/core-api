Agille Park Core API
==============


## Descrição do Projeto ##

***O que é?***

Park Agille é um sistema projetado para empresas prestadoras de serviços na área de estacionamentos rotativo.

***Plataformas***

O sistema foi desenvolvido na plataforma wweb, possibilitando o seu gerenciamento de qualquer lugar do mundo.
Para a inserção de dados o sistema conta com um aplicativo Android.
Todas as conexões serão criptografadas e realizadas com certificado de segurança ssl.

***Linguagem***

WEBSITE: php, html5, css, java e mysql.
APLICATIVO: PhoneGap.


Recursos
Park Agille contara com gerenciamento de níveis e sub níveis de permissões, possibilitando assim que cada Administrador libere apenas as funções do seu interesse para cada grupo de operadores.
## Instalação ##

## Documentação ##
http://redmine.parkagille.com/

## Changelog ##

## Copyright e Licença ##

Copyright (C) Incorpore & WeDev Brasil - Todos os direitos reservados