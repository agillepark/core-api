<?php
/**
 * Agille Park - Estacionamentos Inteligentes - Core-API
 * 
 * Copyright (C) Incorpore & WeDev Brasil - Todos os direitos reservados
 * Copia não autorizada deste arquivo é estritamente proibido
 * Proprietário e Confidencial
 *
 * @link      https://github.com/incorpore/core-api para o repositório privado
 * @copyright Copyright (c) 2015 Incorpore Inc. (http://incorpore.com.br)
 * @author   Thiago R. Moreira <loganguns@gmail.com>
 */
 
namespace Core;

/*
 * Esse arquivo contém configurações para desenvolvimento.
 * Não esqueça de renomea-lo para local.php.dist ou deleta-lo
 * para ambiente de produção
 */
return array(
    
    /**
     * Aplicativos configurados para acessar a API
     *
     * @var array
     */
    'app' => array(
        
        /**
         * Contem informações para autenticar acesso e log de transações 
         */
        '1' => array(
            'name' => 'ClientWeb',
            
            /* Ainda não implementado validação do cliente */
            'url' => 'http://localhost/',
            
            /* Ainda não implementado validação por token */
            'token' => '4e1650a3a4d9a5e8a879011bcecbc262' 
        ),
        
    )
);
