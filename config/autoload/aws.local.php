<?php
/**
 * Agille Park - Estacionamentos Inteligentes - Core-API
 * 
 * Copyright (C) Incorpore & WeDev Brasil - Todos os direitos reservados
 * Copia não autorizada deste arquivo é estritamente proibido
 * Proprietário e Confidencial
 *
 * @link      https://github.com/incorpore/core-api para o repositório privado
 * @copyright Copyright (c) 2015 Incorpore Inc. (http://incorpore.com.br)
 * @author   Thiago R. Moreira <loganguns@gmail.com>
 */

namespace Core;

/*
 * Esse arquivo contém configurações para desenvolvimento.
 * Não esqueça de renomea-lo para local.php.dist ou deleta-lo 
 * para ambiente de produção
 */
 
/**
 * Configuração para AWS-SDK (Módulo AWS)
 */
return array(
    'aws' => array(
        'key'    => 'AKIAIZ2CKGOYBUVUE5NQ',
        'secret' => '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',
        'region' => 'us-east-1a'
    ),

    /**
     * You can alternatively provide a path to an AWS SDK for PHP config file containing your configuration settings.
     * Config files can allow you to have finer-grained control over the configuration settings for each service client.
     */
    'aws' => __DIR__ . '/aws.local.php'
);
