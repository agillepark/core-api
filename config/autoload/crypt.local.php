<?php
/**
 * Agille Park - Estacionamentos Inteligentes - Core-API
 * 
 * Copyright (C) Incorpore & WeDev Brasil - Todos os direitos reservados
 * Copia não autorizada deste arquivo é estritamente proibido
 * Proprietário e Confidencial
 *
 * @link      https://github.com/incorpore/core-api para o repositório privado
 * @copyright Copyright (c) 2015 Incorpore Inc. (http://incorpore.com.br)
 * @author   Thiago R. Moreira <loganguns@gmail.com>
 */
 
namespace Core;

/*
 * Esse arquivo contém configurações para desenvolvimento.
 * Não esqueça de renomea-lo para local.php.dist ou deleta-lo
 * para ambiente de produção
 */
 
/**
* Chave da criptografia concatenada
*
* @var string
*/
$defaultKey = '468e44ddbbba83f7cdb88fa04dc29aca00d6be3ffc3648e4fe';
$defaultKey .= '78702c36cfdca0ec58eef6df07f1f86bebcf91694f4e432a4c';
$defaultKey .= '88449785e427c44d1339de628a1b6bb7dc050464b314a202bd';
$defaultKey .= '3ea554f535fe9431c079ed1115f9838e92b9729f41f73a7df4';
$defaultKey .= '6841a802c5319a66ff7ab90fbf9778b5c251530824225da63b';
$defaultKey .= '82eaf5';

return array(
    
    /**
     * Chave padrão (estática) da criptografia
     *
     * @var string
     */
    'defaultKey' => $defaultKey,
    
    /**
     * Algoritimo de Criptografia
     *
     * @var array
     */
    'algo' => array(
        'algo' => 'aes'
    ),
);
