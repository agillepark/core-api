<?php
/**
 * Agille Park - Estacionamentos Inteligentes - Core-API
 * 
 * Copyright (C) Incorpore & WeDev Brasil - Todos os direitos reservados
 * Copia não autorizada deste arquivo é estritamente proibido
 * Proprietário e Confidencial
 *
 * @link      https://github.com/incorpore/core-api para o repositório privado
 * @copyright Copyright (c) 2015 Incorpore Inc. (http://incorpore.com.br)
 * @author   Thiago R. Moreira <loganguns@gmail.com>
 */
 
namespace Core;

/*
 * Esse arquivo contém configurações para desenvolvimento.
 * Não esqueça de renomea-lo para local.php.dist ou deleta-lo
 * para ambiente de produção
 */
return array(
    
    /**
     * Criptografia na saida de dados da API
     * 
     * @var boolean
     */
    'security' => true, /* False apenas para desenvolvimento e debug
                            Nunca em produção! ) */
    
    /**
     * Formato de saida da API
     * (Apenas Json implementado)
     *
     * @var string
     */
    'format' => 'json',
    
);
