<?php
/**
 * Agille Park - Estacionamentos Inteligentes - Core-API
 * 
 * Copyright (C) Incorpore & WeDev Brasil - Todos os direitos reservados
 * Copia não autorizada deste arquivo é estritamente proibido
 * Proprietário e Confidencial
 *
 * @link      https://github.com/incorpore/core-api para o repositório privado
 * @copyright Copyright (c) 2015 Incorpore Inc. (http://incorpore.com.br)
 * @author   Thiago R. Moreira <loganguns@gmail.com>
 */
 
namespace Core;

/*
 * Esse arquivo contém configurações para ambiente de produção.
 * Não esqueça de renomever o .dist e remover ou renomear o arquivo 'local.php'
 */
return array(
    'doctrine' => array(
        'connection' => array(
            'orm_default' => array(
                'driverClass' => 'Doctrine\DBAL\Driver\PDOMySql\Driver',
                'params' => array(
                    'host' => 'ci.parkagille.com',
                    'port' => '3306',
                    'user' => 'dev_user',
                    'password' => 'NBkjhH(&H7gZ51&*hjg',
                    'dbname' => 'iPark',
                    'charset' => 'utf8',
                    'driverOptions' => array(
                        1002 => 'SET NAMES \'UTF8\''
                    )
                )
            )
        ),
        'driver' => array(
            __NAMESPACE__ . '_annotation_driver' => array(
                'class' => 'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
                'cache' => 'array',
                'paths' => array(
                    __DIR__ . '/../../module/' . __NAMESPACE__ . '/src/' . __NAMESPACE__ . '/Entity'
                )
            ),
            'orm_default' => array(
                'drivers' => array(
                    __NAMESPACE__ . '\Entity' => __NAMESPACE__ . '_annotation_driver'
                )
            )
        )
    )
);