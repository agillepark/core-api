<?php
/**
 * Agille Park - Estacionamentos Inteligentes - Core-API
 *
 * Copyright (C) Incorpore & WeDev Brasil - Todos os direitos reservados
 * Copia não autorizada deste arquivo é estritamente proibido
 * Proprietário e Confidencial
 *
 * @link      https://github.com/incorpore/core-api para o repositório privado
 * @copyright Copyright (c) 2015 Incorpore Inc. (http://incorpore.com.br)
 * @author   Thiago R. Moreira <loganguns@gmail.com>
 */

namespace CoreTest\Controller;

use Zend\Test\PHPUnit\Controller\AbstractHttpControllerTestCase;

class IndexControllerTest extends AbstractHttpControllerTestCase
{
    public function setUp()
    {
        $this->setApplicationConfig(
            include __DIR__ . '/../TestConfig.php.dist'
        );
        parent::setUp();
    }

    public function testIndexActionCanBeAccessed()
    {
        $this->dispatch('/v1');
        $this->assertResponseStatusCode(200);

        $this->assertModuleName('Core');
        $this->assertControllerClass('RestfulController');
    }
}