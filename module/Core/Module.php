<?php
/**
 * Agille Park - Estacionamentos Inteligentes - Core-API
 * Copyright (C) Incorpore & WeDev Brasil - Todos os direitos reservados
 * Copia não autorizada deste arquivo é estritamente proibido
 * Proprietário e Confidencial
 * @link      https://github.com/incorpore/core-api para o repositório privado
 * @copyright Copyright (c) 2015 Incorpore Inc. (http://incorpore.com.br)
 * @author    Thiago R. Moreira <loganguns@gmail.com>
 */

namespace Core;

use Core\PreProcessor\PreProcessor;
use Zend\Mvc\ModuleRouteListener;
use Zend\Mvc\MvcEvent;
use Zend\View\Model\JsonModel;
use Core\Authentication\Adapter\HeaderAuthentication;

/**
 * Module
 *
 * @category Core
 * @package  Module
 * @author   Thiago R. Moreira <loganguns@gmail.com>
 */
class Module
{

    /**
     * @param MvcEvent $e
     *
     * @return void
     */
    public function onBootstrap(MvcEvent $e)
    {
        /** @var \Zend\EventManager\EventManager $moduleManager */
        $eventManager = $e->getApplication()->getEventManager();

        /** @var \Zend\ModuleManager\ModuleManager $serviceManager */
        $serviceLocator = $e->getApplication()->getServiceManager();

        /** @var \Zend\ModuleManager\ModuleManager $moduleManager */
        $moduleManager = $e->getApplication()->getServiceManager()
            ->get('modulemanager');

        /** @var \Zend\EventManager\SharedEventManager $sharedEvents */
        $sharedManager = $moduleManager->getEventManager()->getSharedManager();

        $request = $e->getRequest();
        $moduleRouteListener = new ModuleRouteListener();
        $moduleRouteListener->attach($eventManager);
        $eventManager->attach(
            MvcEvent::EVENT_DISPATCH_ERROR,
            [
                $this,
                'onDispatchError'
            ], 0
        );
        $eventManager->attach(
            MvcEvent::EVENT_RENDER_ERROR,
            [
                $this,
                'onRenderError'
            ], 0
        );


        /*$sharedManager->attach(
            'Core\Controller\RestfulController',
            MvcEvent::EVENT_DISPATCH,
            array(new HeaderAuthentication($request), 'process'),
            //[$this, 'initAuthtentication'],
            100
        );*/


        //adiciona eventos ao módulo
        //pré e pós-processadores do controller Rest
        /*$sharedEvents->attach(
            'Core\Controller\RestfulController',
            MvcEvent::EVENT_DISPATCH,
            array(new PostProcessor(), 'process'),
            -100
        );
        */

        $sharedManager->attach(
            'Core\Controller\AuthenticateController',
            MvcEvent::EVENT_DISPATCH,
            [new PreProcessor($e), 'validate'],
            100
        );
        /*
        $sharedManager->attach(
            'Core\Controller\RestfulController',
            MvcEvent::EVENT_DISPATCH,
            array(new PreProcessor($e), 'process'),// @TODO Change function call
            100
        );
        */

    }

    /**
     * @param MvcEvent $e
     *
     * @return void|JsonModel
     */
    public function onDispatchError(MvcEvent $e)
    {
        return $this->getJsonModelError($e);
    }

    /**
     * @param MvcEvent $e
     *
     * @return void|JsonModel
     */
    public function onRenderError(MvcEvent $e)
    {
        return $this->getJsonModelError($e);
    }

    /**
     * @param MvcEvent $e
     *
     * @return void|\Zend\View\Model\JsonModel
     */
    public function getJsonModelError(MvcEvent $e)
    {
        $error = $e->getError();
        if (! $error) {
            return true;
        }
        $response = $e->getResponse();
        $exception = $e->getParam('exception');
        $exceptionJson = [];
        if ($exception) {
            $exceptionJson = [
                'class' => get_class($exception),
                'file' => $exception->getFile(),
                'line' => $exception->getLine(),
                'message' => $exception->getMessage(),
                'stacktrace' => $exception->getTraceAsString()
            ];
        }
        $errorJson = [
            'message' => 'An error occurred during execution; please try again later.',
            'error' => $error,
            'exception' => $exceptionJson
        ];
        if ($error == 'error-router-no-match') {
            $errorJson['message'] = 'Resource not found.';
        }
        $model = new JsonModel(
            [
                'errors' => [
                    $errorJson
                ]
            ]
        );
        $e->setResult($model);
        return $model;
    }

    /**
     *
     */
    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    /**
     * @return multitype:multitype:multitype:string
     */
    public function getAutoloaderConfig()
    {
        return [
            'Zend\Loader\StandardAutoloader' => [
                'namespaces' => [
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__
                ]
            ]
        ];
    }

    /**
     * @param MvcEvent $e
     *
     * @return mixed
     */
    public function initAuthtentication(MvcEvent $e)
    {
        /*
         * @var MvcEvent $e
         */
        $request = $e->getRequest();
        $response = $e->getResponse();
        $view = $e->getApplication()->getMvcEvent()->getViewModel();
        $sm = $e->getApplication()->getServiceManager();
        $authAdapter = $sm->get('Core\DigestAuthenticationAdapter');

        $authAdapter->setRequest($request);
        $authAdapter->setResponse($response);

        $result = $authAdapter->authenticate();

        /*
         * Pass the information to the view and see what we got
         */
        if (!$result->isValid()) {
            /*
             * Create a log function or just use the one from LearnZF2.
             * Also make sure to redirect to another page, 404 for example
             */
            foreach ($result->getMessages() as $msg) {
                $view->authProblem = $msg;
            }
            return $view->authProblem;
        }

        return $view->identity = $result->getIdentity();
    }
}
