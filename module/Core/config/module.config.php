<?php
/**
 * Agille Park - Estacionamentos Inteligentes - Core-API
 * Copyright (C) Incorpore & WeDev Brasil - Todos os direitos reservados
 * Copia não autorizada deste arquivo é estritamente proibido
 * Proprietário e Confidencial

 * @link      https://github.com/incorpore/core-api para o repositório privado
 * @copyright Copyright (c) 2015 Incorpore Inc. (http://incorpore.com.br)
 * @author    Thiago R. Moreira <loganguns@gmail.com>
 */

namespace Core;

use Doctrine\ORM\EntityManager;

return [
    // The following section is new` and should be added to your file
    'router' => [
        'routes' => [
            'core-api' => [
                'type' => 'Literal',
                'options' => [
                    'route' => '/v1',
                    'defaults' => [
                        '__NAMESPACE__' => 'Core\Controller',
                        'controller' => 'Restful'
                    ]
                ],
                'may_terminate' => true,
                'child_routes' => [
                    'default' => [
                        'type' => 'Segment',
                        'options' => [
                            'route' => '/:service[/][:id][/]',
                            'constraints' => [
                                'formatter' => '[a-zA-Z]+',
                            ],
                            'defaults' => [
                                'action' => null,
                                'service' => null,
                            ]
                        ]
                    ]
                ]
            ],
            'authenticateRequest' => [
                'type' => 'Literal',
                'options' => [
                    'route' => '/v1/getAuthorization',
                    'defaults' => [
                        '__NAMESPACE__' => 'Core\Controller',
                        'controller' => 'Authenticate',
                    ]
                ]
            ]
        ]
    ],
    'view_manager' => [
        'strategies' => [
            'ViewJsonStrategy'
        ]
    ],
    
    // Invokables Controllers
    'controllers' => [
        'invokables' => [
            'Core\Controller\Restful' => 'Core\Controller\RestfulController',
            'Core\Controller\Authenticate' => 'Core\Controller\AuthenticateController',
        ]
    ],
    
    'doctrine' => [
        'driver' => [
            __NAMESPACE__ . '_annotation_driver' => [
                'class' => 'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
                'cache' => 'array',
                'paths' => [
                    __DIR__ . '/../src/' . __NAMESPACE__ . '/Entity'
                ]
            ],
            'orm_default' => [
                'drivers' => [
                    __NAMESPACE__ . '\Entity' => __NAMESPACE__ . '_annotation_driver'
                ]
            ]
        ]
    ],
    
    'jms_serializer' => [
        'metadata' => [
            'directories' => [
                'any-name' => [
                    'namespace_prefix' => 'Core\Entity',
                    'path' => __DIR__ . '/serializer'
                ]
            ]
        ]
    ],
    
    
    'authentication_digest' => [
        'adapter' => [
            'config' => [
                'accept_schemes' => 'digest',
                'realm' => 'authentication',
                'digest_domains' => '/v1',
                'nonce_timeout' => 3600,
            ],
            'digest' => __DIR__ . '/auth/digest.txt',
        ],
    ],
    'service_manager' => [
        'factories' => [
            'Core\DigestAuthenticationAdapter' => 'Core\Factory\DigestAuthenticationAdapterFactory',
        ],
    ],


];
