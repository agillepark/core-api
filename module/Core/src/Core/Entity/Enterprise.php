<?php
/**
 * Agille Park - Estacionamentos Inteligentes - Core-API
 * Copyright (C) Incorpore & WeDev Brasil - Todos os direitos reservados
 * Copia não autorizada deste arquivo é estritamente proibido
 * Proprietário e Confidencial
 * @link      https://github.com/incorpore/core-api para o repositório privado
 * @copyright Copyright (c) 2015 Incorpore Inc. (http://incorpore.com.br)
 * @author    Thiago R. Moreira <loganguns@gmail.com>
 */
 
namespace Core\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Enterprise

 * @ORM\Table(
 *  name="enterprise",
 *  indexes={
 *      @ORM\Index(
 *          name="fk_enterprise_address1_idx",
 *          columns={"address_id"}),
 *              @ORM\Index(
 *                  name="fk_enterprise_enterprise_type1_idx",
 *                  columns={"enterprise_type_enterprise_type_id"}
 *              )
 *          }
 *      )
 * @ORM\Entity
 */
class Enterprise
{

    /**
     * @var integer
     * @ORM\Column(name="enterprise_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $enterpriseId;

    /**
     * @var string
     * @ORM\Column(name="name", type="string", length=45, nullable=true)
     */
    private $name;

    /**
     * @var \DateTime
     * @ORM\Column(name="create_date", type="datetime", nullable=false)
     */
    private $createDate;

    /**
     * @var \DateTime
     * @ORM\Column(name="last_update", type="datetime", nullable=true)
     */
    private $lastUpdate;

    /**
     * @var \Core\Entity\EnterpriseType
     * @ORM\ManyToOne(targetEntity="Core\Entity\EnterpriseType")
     * @ORM\JoinColumns({
     *  @ORM\JoinColumn(
     *      name="enterprise_type_enterprise_type_id",
     *      referencedColumnName="enterprise_type_id"
     *  )
     * })
     */
    private $enterpriseTypeEnterpriseType;

    /**
     * @var \Core\Entity\Address
     * @ORM\ManyToOne(targetEntity="Core\Entity\Address")
     * @ORM\JoinColumns({
     *  @ORM\JoinColumn(name="address_id", referencedColumnName="address_id")
     * })
     */
    private $address;

    /**
     * @var \Doctrine\Common\Collections\Collection
     * @ORM\ManyToMany(
     *  targetEntity="Core\Entity\Contact",
     *  inversedBy="enterprise"
     * )
     * @ORM\JoinTable(
     *  name="enterprise_has_contact",
     *  joinColumns={
     *      @ORM\JoinColumn(
     *      name="enterprise_enterprise_id",
     *      referencedColumnName="enterprise_id"
     *   )
     *  },
     *  inverseJoinColumns={
     *      @ORM\JoinColumn(
     *      name="contact_contact_id",
     *      referencedColumnName="contact_id"
     *   )
     *  }
     * )
     */
    private $contact;

    /**
     * @var \Doctrine\Common\Collections\Collection
     * @ORM\ManyToMany(
     *  targetEntity="Core\Entity\User",
     *  inversedBy="enterprise"
     * )
     * @ORM\JoinTable(
     *  name="enterprise_has_user",
     *  joinColumns={
     *      @ORM\JoinColumn(
     *          name="enterprise_enterprise_id",
     *          referencedColumnName="enterprise_id"
     *      )
     *  },
     *  inverseJoinColumns={
     *      @ORM\JoinColumn(
     *          name="user_user_id",
     *          referencedColumnName="user_id"
     *      )
     *  }
     * )
     */
    private $user;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->contact = new ArrayCollection();
        $this->user = new ArrayCollection();
    }

    /**
     * Get enterpriseId
     *
     * @return integer
     */
    public function getEnterpriseId()
    {
        return $this->enterpriseId;
    }

    /**
     * Set name
     *
     * @param string $name            
     *
     * @return Enterprise
     */
    public function setName($name)
    {
        $this->name = $name;
        
        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set createDate
     *
     * @param \DateTime $createDate            
     *
     * @return Enterprise
     */
    public function setCreateDate($createDate)
    {
        $this->createDate = $createDate;
        
        return $this;
    }

    /**
     * Get createDate
     *
     * @return \DateTime
     */
    public function getCreateDate()
    {
        return $this->createDate;
    }

    /**
     * Set lastUpdate
     *
     * @param \DateTime $lastUpdate            
     *
     * @return Enterprise
     */
    public function setLastUpdate($lastUpdate)
    {
        $this->lastUpdate = $lastUpdate;
        
        return $this;
    }

    /**
     * Get lastUpdate
     *
     * @return \DateTime
     */
    public function getLastUpdate()
    {
        return $this->lastUpdate;
    }

    /**
     * Set enterpriseTypeEnterpriseType
     *
     * @param \Core\Entity\EnterpriseType $enterpriseTypeEnterpriseType            
     *
     * @return Enterprise
     */
    public function setEnterpriseTypeEnterpriseType(
        EnterpriseType $enterpriseTypeEnterpriseType = null
    )
    {
        $this->enterpriseTypeEnterpriseType = $enterpriseTypeEnterpriseType;
        
        return $this;
    }

    /**
     * Get enterpriseTypeEnterpriseType
     *
     * @return \Core\Entity\EnterpriseType
     */
    public function getEnterpriseTypeEnterpriseType()
    {
        return $this->enterpriseTypeEnterpriseType;
    }

    /**
     * Set address
     *
     * @param \Core\Entity\Address $address            
     *
     * @return Enterprise
     */
    public function setAddress(Address $address = null)
    {
        $this->address = $address;
        
        return $this;
    }

    /**
     * Get address
     *
     * @return \Core\Entity\Address
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Add contact
     *
     * @param \Core\Entity\Contact $contact            
     *
     * @return Enterprise
     */
    public function addContact(Contact $contact)
    {
        $this->contact[] = $contact;
        
        return $this;
    }

    /**
     * Remove contact
     *
     * @param \Core\Entity\Contact $contact            
     */
    public function removeContact(Contact $contact)
    {
        $this->contact->removeElement($contact);
    }

    /**
     * Get contact
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getContact()
    {
        return $this->contact;
    }

    /**
     * Add user
     *
     * @param \Core\Entity\User $user            
     *
     * @return Enterprise
     */
    public function addUser(User $user)
    {
        $this->user[] = $user;
        
        return $this;
    }

    /**
     * Remove user
     *
     * @param \Core\Entity\User $user            
     */
    public function removeUser(User $user)
    {
        $this->user->removeElement($user);
    }

    /**
     * Get user
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getUser()
    {
        return $this->user;
    }
}
