<?php
/**
 * Agille Park - Estacionamentos Inteligentes - Core-API
 * Copyright (C) Incorpore & WeDev Brasil - Todos os direitos reservados
 * Copia não autorizada deste arquivo é estritamente proibido
 * Proprietário e Confidencial
 * @link      https://github.com/incorpore/core-api para o repositório privado
 * @copyright Copyright (c) 2015 Incorpore Inc. (http://incorpore.com.br)
 * @author    Thiago R. Moreira <loganguns@gmail.com>
 */
 
namespace Core\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * State

 * @ORM\Table(
 *  name="state",
 *  indexes={
 *      @ORM\Index(
 *          name="fk_state_country_idx",
 *          columns={"country_country_id"}
 *      )
 *  }
 * )
 * @ORM\Entity
 */
class State
{

    /**
     * @var integer
     * @ORM\Column(name="state_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $stateId;

    /**
     * @var string
     * @ORM\Column(name="name", type="string", length=45, nullable=true)
     */
    private $name;

    /**
     * @var string
     * @ORM\Column(name="acronym", type="string", length=45, nullable=true)
     */
    private $acronym;

    /**
     * @var \Core\Entity\Country
     * @ORM\ManyToOne(targetEntity="Core\Entity\Country")
     * @ORM\JoinColumns({
     *  @ORM\JoinColumn(
     *      name="country_country_id",
     *      referencedColumnName="country_id"
     *  )
     * })
     */
    private $countryCountry;

    /**
     * Get stateId
     *
     * @return integer
     */
    public function getStateId()
    {
        return $this->stateId;
    }

    /**
     * Set name
     *
     * @param string $name            
     *
     * @return State
     */
    public function setName($name)
    {
        $this->name = $name;
        
        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set acronym
     *
     * @param string $acronym            
     *
     * @return State
     */
    public function setAcronym($acronym)
    {
        $this->acronym = $acronym;
        
        return $this;
    }

    /**
     * Get acronym
     *
     * @return string
     */
    public function getAcronym()
    {
        return $this->acronym;
    }

    /**
     * Set countryCountry
     *
     * @param \Core\Entity\Country $countryCountry            
     *
     * @return State
     */
    public function setCountryCountry(
        Country $countryCountry = null
    )
    {
        $this->countryCountry = $countryCountry;
        
        return $this;
    }

    /**
     * Get countryCountry
     *
     * @return \Core\Entity\Country
     */
    public function getCountryCountry()
    {
        return $this->countryCountry;
    }
}
