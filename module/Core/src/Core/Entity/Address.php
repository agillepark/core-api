<?php
/**
 * Agille Park - Estacionamentos Inteligentes - Core-API
 * Copyright (C) Incorpore & WeDev Brasil - Todos os direitos reservados
 * Copia não autorizada deste arquivo é estritamente proibido
 * Proprietário e Confidencial
 * @link      https://github.com/incorpore/core-api para o repositório privado
 * @copyright Copyright (c) 2015 Incorpore Inc. (http://incorpore.com.br)
 * @author    Thiago R. Moreira <loganguns@gmail.com>
 */
 
namespace Core\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Address

 * @ORM\Table(
 *      name="address",
 *      indexes={
 *          @ORM\Index(
 *              name="fk_address_city1_idx",
 *              columns={"city_city_id"}
 *          ),
 *          @ORM\Index(
 *              name="fk_address_address_type1_idx",
 *              columns={"address_type_address_type_id"}
 *          )
 *      }
 *  )
 * @ORM\Entity
 */
class Address
{

    /**
     * @var integer
     * @ORM\Column(
     *  name="address_id", type="integer", nullable=false
     * )
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $addressId;

    /**
     * @var string
     * @ORM\Column(
     *  name="address", type="string", length=45, nullable=true
     * )
     */
    private $address;

    /**
     * @var string
     * @ORM\Column(
     *  name="district_name", type="string", length=45, nullable=true
     * )
     */
    private $districtName;

    /**
     * @var string
     * @ORM\Column(
     *  name="postal_code", type="string", length=45, nullable=true
     * )
     */
    private $postalCode;

    /**
     * @var \Core\Entity\City
     * @ORM\ManyToOne(targetEntity="Core\Entity\City")
     * @ORM\JoinColumns({
     *  @ORM\JoinColumn(name="city_city_id", referencedColumnName="city_id")
     * })
     */
    private $cityCity;

    /**
     * @var \Core\Entity\AddressType
     * @ORM\ManyToOne(
     *  targetEntity="Core\Entity\AddressType"
     * )
     * @ORM\JoinColumns({
     *  @ORM\JoinColumn(
     *      name="address_type_address_type_id",
     *      referencedColumnName="address_type_id"
     *  )
     * })
     */
    private $addressTypeAddressType;

    /**
     * Get addressId
     *
     * @return integer
     */
    public function getAddressId()
    {
        return $this->addressId;
    }

    /**
     * Set address
     *
     * @param string $address            
     *
     * @return Address
     */
    public function setAddress($address)
    {
        $this->address = $address;
        
        return $this;
    }

    /**
     * Get address
     *
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set districtName
     *
     * @param string $districtName            
     *
     * @return Address
     */
    public function setDistrictName($districtName)
    {
        $this->districtName = $districtName;
        
        return $this;
    }

    /**
     * Get districtName
     *
     * @return string
     */
    public function getDistrictName()
    {
        return $this->districtName;
    }

    /**
     * Set postalCode
     *
     * @param string $postalCode            
     *
     * @return Address
     */
    public function setPostalCode($postalCode)
    {
        $this->postalCode = $postalCode;
        
        return $this;
    }

    /**
     * Get postalCode
     *
     * @return string
     */
    public function getPostalCode()
    {
        return $this->postalCode;
    }

    /**
     * Set cityCity
     *
     * @param \Core\Entity\City $cityCity            
     *
     * @return Address
     */
    public function setCityCity(City $cityCity = null)
    {
        $this->cityCity = $cityCity;
        
        return $this;
    }

    /**
     * Get cityCity
     *
     * @return \Core\Entity\City
     */
    public function getCityCity()
    {
        return $this->cityCity;
    }

    /**
     * Set addressTypeAddressType
     *
     * @param \Core\Entity\AddressType $addressTypeAddressType            
     *
     * @return Address
     */
    public function setAddressTypeAddressType(AddressType
                                              $addressTypeAddressType = null)
    {
        $this->addressTypeAddressType = $addressTypeAddressType;
        
        return $this;
    }

    /**
     * Get addressTypeAddressType
     *
     * @return \Core\Entity\AddressType
     */
    public function getAddressTypeAddressType()
    {
        return $this->addressTypeAddressType;
    }
}
