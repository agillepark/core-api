<?php
/**
 * Agille Park - Estacionamentos Inteligentes - Core-API
 * Copyright (C) Incorpore & WeDev Brasil - Todos os direitos reservados
 * Copia não autorizada deste arquivo é estritamente proibido
 * Proprietário e Confidencial
 * @link      https://github.com/incorpore/core-api para o repositório privado
 * @copyright Copyright (c) 2015 Incorpore Inc. (http://incorpore.com.br)
 * @author    Thiago R. Moreira <loganguns@gmail.com>
 */
 
namespace Core\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Vacancy

 * @ORM\Table(
 *  name="vacancy",
 *  indexes={
 *      @ORM\Index(
 *          name="fk_vacancy_vacancy_type1_idx",
 *          columns={"vacancy_type_vacancy_type_id"}),
 *          @ORM\Index(name="fk_vacancy_city1_idx",
 *          columns={"city_city_id"}
 *      )
 *  }
 * )
 * @ORM\Entity
 */
class Vacancy
{

    /**
     * @var integer
     * @ORM\Column(name="vacancy_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $vacancyId;

    /**
     * @var integer
     * @ORM\Column(name="number", type="integer", nullable=true)
     */
    private $number;

    /**
     * @var string
     * @ORM\Column(name="status", type="string", length=45, nullable=true)
     */
    private $status;

    /**
     * @var \Core\Entity\VacancyType
     * @ORM\ManyToOne(targetEntity="Core\Entity\VacancyType", fetch="EAGER")
     * @ORM\JoinColumns({
     *  @ORM\JoinColumn(
     *      name="vacancy_type_vacancy_type_id",
     *      referencedColumnName="vacancy_type_id"
     *  )
     * })
     */
    private $vacancyType;

    /**
     * @var \Core\Entity\City
     * @ORM\ManyToOne(targetEntity="Core\Entity\City")
     * @ORM\JoinColumns({
     *  @ORM\JoinColumn(name="city_city_id", referencedColumnName="city_id")
     * })
     */
    private $city;

    /**
     * Get vacancyId
     *
     * @return integer
     */
    public function getVacancyId()
    {
        return $this->vacancyId;
    }

    /**
     * Set number
     *
     * @param integer $number            
     *
     * @return Vacancy
     */
    public function setNumber($number)
    {
        $this->number = $number;
        
        return $this;
    }

    /**
     * Get number
     *
     * @return integer
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * Set status
     *
     * @param string $status            
     *
     * @return Vacancy
     */
    public function setStatus($status)
    {
        $this->status = $status;
        
        return $this;
    }

    /**
     * Get status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set vacancyType
     *
     * @param \Core\Entity\VacancyType $vacancyType            
     *
     * @return Vacancy
     */
    public function setVacancyType(VacancyType $vacancyType = null)
    {
        $this->vacancyType = $vacancyType;
        
        return $this;
    }

    /**
     * Get vacancyType
     *
     * @return \Core\Entity\VacancyType
     */
    public function getVacancyType()
    {
        return $this->vacancyType;
    }

    /**
     * Set city
     *
     * @param \Core\Entity\City $city            
     *
     * @return Vacancy
     */
    public function setCityCity(City $city = null)
    {
        $this->city = $city;
        
        return $this;
    }

    /**
     * Get city
     *
     * @return \Core\Entity\City
     */
    public function getCityCity()
    {
        return $this->city;
    }
}
