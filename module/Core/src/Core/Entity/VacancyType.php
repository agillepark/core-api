<?php
/**
 * Agille Park - Estacionamentos Inteligentes - Core-API
 * Copyright (C) Incorpore & WeDev Brasil - Todos os direitos reservados
 * Copia não autorizada deste arquivo é estritamente proibido
 * Proprietário e Confidencial
 * @link      https://github.com/incorpore/core-api para o repositório privado
 * @copyright Copyright (c) 2015 Incorpore Inc. (http://incorpore.com.br)
 * @author    Thiago R. Moreira <loganguns@gmail.com>
 */
 
namespace Core\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * VacancyType
 *
 * @ORM\Table(name="vacancy_type")
 * @ORM\Entity
 */
class VacancyType
{

    /**
     * @var integer
     * @ORM\Column(name="vacancy_type_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $vacancyTypeId;

    /**
     * @var string
     * @ORM\Column(name="name", type="string", length=45, nullable=true)
     */
    private $name;

    /**
     * Get vacancyTypeId
     *
     * @return integer
     */
    public function getVacancyTypeId()
    {
        return $this->vacancyTypeId;
    }

    /**
     * Set name
     *
     * @param string $name
     *
*@return VacancyType
     */
    public function setName($name)
    {
        $this->name = $name;
        
        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
}
