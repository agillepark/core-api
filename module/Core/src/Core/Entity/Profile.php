<?php
/**
 * Agille Park - Estacionamentos Inteligentes - Core-API
 * Copyright (C) Incorpore & WeDev Brasil - Todos os direitos reservados
 * Copia não autorizada deste arquivo é estritamente proibido
 * Proprietário e Confidencial
 * @link      https://github.com/incorpore/core-api para o repositório privado
 * @copyright Copyright (c) 2015 Incorpore Inc. (http://incorpore.com.br)
 * @author    Thiago R. Moreira <loganguns@gmail.com>
 */
 
namespace Core\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Profile

 * @ORM\Table(
 *  name="profile",
 *  indexes={
 *      @ORM\Index(
 *          name="fk_profile_address1_idx",
 *          columns={"address_address_id"}
 *      )
 *  }
 * )
 * @ORM\Entity
 */
class Profile
{

    /**
     * @var integer
     * @ORM\Column(name="profile_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $profileId;

    /**
     * @var string
     * @ORM\Column(name="first_name", type="string", length=45, nullable=true)
     */
    private $firstName;

    /**
     * @var string
     * @ORM\Column(name="last_name", type="string", length=45, nullable=true)
     */
    private $lastName;

    /**
     * @var \DateTime
     * @ORM\Column(name="born_date", type="date", nullable=true)
     */
    private $bornDate;

    /**
     * @ORM\OneToOne(targetEntity="Core\Entity\User", inversedBy="profile")
     */
    private $user;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->contact = new ArrayCollection();
    }

    /**
     * Get profileId
     *
     * @return integer
     */
    public function getProfileId()
    {
        return $this->profileId;
    }

    /**
     * Set firstName
     *
     * @param string $firstName            
     *
     * @return Profile
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;
        
        return $this;
    }

    /**
     * Get firstName
     *
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * Set lastName
     *
     * @param string $lastName            
     *
     * @return Profile
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;
        
        return $this;
    }

    /**
     * Get lastName
     *
     * @return string
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * Add contact
     *
     * @param \Core\Entity\Contact $contact            
     *
     * @return Profile
     */
    public function addContact(Contact $contact)
    {
        $this->contact[] = $contact;
        
        return $this;
    }

    /**
     * Set bornDate
     *
     * @param \Date $bornDate            
     *
     * @return Profile
     */
    public function setBornDate($bornDate)
    {
        $this->bornDate = $bornDate;
        
        return $this;
    }

    /**
     * Get bornDate
     *
     * @return \Date
     */
    public function getBornDate()
    {
        return $this->bornDate;
    }

    /**
     * Remove contact
     *
     * @param \Core\Entity\Contact $contact            
     */
    public function removeContact(Contact $contact)
    {
        $this->contact->removeElement($contact);
    }

    /**
     * Get contact
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getContact()
    {
        return $this->contact;
    }

    /**
     * Set user
     *
     * @param string $user            
     *
     * @return Profile
     */
    public function setUser($user)
    {
        $this->user = $user;
        
        return $this;
    }

    /**
     * Get user
     *
     * @return string
     */
    public function getUser()
    {
        return $this->user;
    }
}
