<?php
/**
 * Agille Park - Estacionamentos Inteligentes - Core-API
 * Copyright (C) Incorpore & WeDev Brasil - Todos os direitos reservados
 * Copia não autorizada deste arquivo é estritamente proibido
 * Proprietário e Confidencial
 * @link      https://github.com/incorpore/core-api para o repositório privado
 * @copyright Copyright (c) 2015 Incorpore Inc. (http://incorpore.com.br)
 * @author    Thiago R. Moreira <loganguns@gmail.com>
 */
 
namespace Core\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * EnterpriseType
 *
 * @ORM\Table(name="enterprise_type")
 * @ORM\Entity
 */
class EnterpriseType
{

    /**
     * @var integer
     * @ORM\Column(
     *  name="enterprise_type_id",
     *  type="integer",
     *  nullable=false
     * )
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $enterpriseTypeId;

    /**
     * @var string @ORM\Column(
     *  name="name",
     *  type="string",
     *  length=45,
     *  nullable=true
     * )
     */
    private $name;

    /**
     * Get enterpriseTypeId
     *
     * @return integer
     */
    public function getEnterpriseTypeId()
    {
        return $this->enterpriseTypeId;
    }

    /**
     * Set name
     *
     * @param string $name            
     *
     * @return EnterpriseType
     */
    public function setName($name)
    {
        $this->name = $name;
        
        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
}
