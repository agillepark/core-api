<?php
/**
 * Agille Park - Estacionamentos Inteligentes - Core-API
 * Copyright (C) Incorpore & WeDev Brasil - Todos os direitos reservados
 * Copia não autorizada deste arquivo é estritamente proibido
 * Proprietário e Confidencial
 * @link      https://github.com/incorpore/core-api para o repositório privado
 * @copyright Copyright (c) 2015 Incorpore Inc. (http://incorpore.com.br)
 * @author    Thiago R. Moreira <loganguns@gmail.com>
 */
 
namespace Core\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ContactType
 *
 * @ORM\Table(name="contact_type")
 * @ORM\Entity
 */
class ContactType
{

    /**
     * @var integer
     * @ORM\Column(name="contact_type_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $contactTypeId;

    /**
     * @var string
     * @ORM\Column(
     *  name="name",
     *  type="string",
     *  length=45,
     *  nullable=true
     * )
     */
    private $name;

    /**
     * Get contactTypeId
     *
     * @return integer
     */
    public function getContactTypeId()
    {
        return $this->contactTypeId;
    }

    /**
     * Set name
     *
     * @param string $name            
     *
     * @return ContactType
     */
    public function setName($name)
    {
        $this->name = $name;
        
        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
}
