<?php
/**
 * Agille Park - Estacionamentos Inteligentes - Core-API
 * 
 * Copyright (C) Incorpore & WeDev Brasil - Todos os direitos reservados
 * Copia não autorizada deste arquivo é estritamente proibido
 * Proprietário e Confidencial
 *
 * @link      https://github.com/incorpore/core-api para o repositório privado
 * @copyright Copyright (c) 2015 Incorpore Inc. (http://incorpore.com.br)
 * @author   Thiago R. Moreira <loganguns@gmail.com>
 */
 
namespace Core\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * City
 *
 * @ORM\Table(
 *  name="city",
 *  indexes={
 *      @ORM\Index(
 *          name="fk_city_state1_idx",
 *          columns={"state_state_id"}),
 *          @ORM\Index(name="fk_city_enterprise1_idx",
 *          columns={"enterprise_enterprise_id"}
 *      )
 *  }
 * )
 * @ORM\Entity
 */
class City
{

    /**
     * @var integer
     * @ORM\Column(name="city_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $cityId;

    /**
     * @var string
     * @ORM\Column(name="name", type="string", length=45, nullable=true)
     */
    private $name;

    /**
     * @var \Core\Entity\State
     * @ORM\ManyToOne(targetEntity="Core\Entity\State")
     * @ORM\JoinColumns({
     *  @ORM\JoinColumn(name="state_state_id", referencedColumnName="state_id")
     * })
     */
    private $stateState;

    /**
     * @ORM\OneToOne(targetEntity="Core\Entity\Vacancy", inversedBy="city")
     */
    private $vacancy;

    /**
     * @var \Core\Entity\Enterprise
     * @ORM\ManyToOne(targetEntity="Core\Entity\Enterprise", fetch="EXTRA_LAZY")
     * @ORM\JoinColumns({
     *  @ORM\JoinColumn(
     *      name="enterprise_enterprise_id",
     *      referencedColumnName="enterprise_id"
     *  )
     * })
     */
    private $enterprise;

    /**
     * Get cityId
     *
     * @return integer
     */
    public function getCityId()
    {
        return $this->cityId;
    }

    /**
     * Set name
     *
     * @param string $name            
     *
     * @return City
     */
    public function setName($name)
    {
        $this->name = $name;
        
        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set stateState
     *
     * @param \Core\Entity\State $stateState            
     *
     * @return City
     */
    public function setStateState(State $stateState = null)
    {
        $this->stateState = $stateState;
        
        return $this;
    }

    /**
     * Get stateState
     *
     * @return \Core\Entity\State
     */
    public function getStateState()
    {
        return $this->stateState;
    }

    /**
     * Set enterprise
     *
     * @param \Core\Entity\Enterprise $enterprise            
     *
     * @return City
     */
    public function setEnterprise(Enterprise $enterprise = null)
    {
        $this->enterprise = $enterprise;
        
        return $this;
    }

    /**
     * Get enterprise
     *
     * @return \Core\Entity\Enterprise
     */
    public function getEnterprise()
    {
        return $this->enterprise;
    }

    /**
     * Set vacancy
     *
     * @param string $vacancy            
     *
     * @return City
     */
    public function setVacancy($vacancy)
    {
        $this->vacancy = $vacancy;
        
        return $this;
    }

    /**
     * Get vacancy
     *
     * @return string
     */
    public function getVacancy()
    {
        return $this->vacancy;
    }
}
