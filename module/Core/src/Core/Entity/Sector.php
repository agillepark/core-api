<?php
/**
 * Agille Park - Estacionamentos Inteligentes - Core-API
 * Copyright (C) Incorpore & WeDev Brasil - Todos os direitos reservados
 * Copia não autorizada deste arquivo é estritamente proibido
 * Proprietário e Confidencial
 * @link      https://github.com/incorpore/core-api para o repositório privado
 * @copyright Copyright (c) 2015 Incorpore Inc. (http://incorpore.com.br)
 * @author    Thiago R. Moreira <loganguns@gmail.com>
 */
 
namespace Core\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Sector

 * @ORM\Table(
 *  name="sector",
 *  indexes={
 *      @ORM\Index(
 *          name="fk_sector_sector_type1_idx",
 *          columns={"sector_type_sector_type_id"}),
 *          @ORM\Index(name="fk_sector_city1_idx", columns={"city_city_id"})
 *      }
 *  )
 * @ORM\Entity
 */
class Sector
{

    /**
     * @var integer
     * @ORM\Column(name="sector_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $sectorId;

    /**
     * @var integer
     * @ORM\Column(name="name", type="integer", nullable=true)
     */
    private $name;

    /**
     * @var string
     * @ORM\Column(name="description", type="string", length=45, nullable=true)
     */
    private $description;

    /**
     * @var \Core\Entity\VacancyType
     * @ORM\ManyToOne(targetEntity="Core\Entity\VacancyType", fetch="EAGER")
     * @ORM\JoinColumns({
     *  @ORM\JoinColumn(
     *      name="vacancy_type_vacancy_type_id",
     *      referencedColumnName="vacancy_type_id"
     *  )
     * })
     */
    private $vacancyType;

    /**
     * @var \Core\Entity\City
     * @ORM\ManyToOne(targetEntity="Core\Entity\City")
     * @ORM\JoinColumns({
     *      @ORM\JoinColumn(name="city_city_id", referencedColumnName="city_id")
     * })
     */
    private $city;

    /**
     * Get sectorId
     *
     * @return integer
     */
    public function getSectorId()
    {
        return $this->sectorId;
    }

    /**
     * Set name
     *
     * @param integer $name            
     *
     * @return Sector
     */
    public function setName($name)
    {
        $this->name = $name;
        
        return $this;
    }

    /**
     * Get name
     *
     * @return integer
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description            
     *
     * @return Sector
     */
    public function setDescription($description)
    {
        $this->description = $description;
        
        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set vacancyType
     *
     * @param VacancyType $vacancyType
     *
     * @return Sector
     * @internal param VacancyType $sectorType
     */
    public function setVacancyType(VacancyType $vacancyType = null)
    {
        $this->vacancyType = $vacancyType;
        
        return $this;
    }

    /**
     * Get vacancyType
     *
     * @return \Core\Entity\VacancyType
     */
    public function getVacancyType()
    {
        return $this->vacancyType;
    }

    /**
     * Set city
     *
     * @param \Core\Entity\City $city            
     *
     * @return Sector
     */
    public function setCityCity(City $city = null)
    {
        $this->city = $city;
        
        return $this;
    }

    /**
     * Get city
     *
     * @return \Core\Entity\City
     */
    public function getCityCity()
    {
        return $this->city;
    }
}
