<?php
/**
 * Agille Park - Estacionamentos Inteligentes - Core-API
 * Copyright (C) Incorpore & WeDev Brasil - Todos os direitos reservados
 * Copia não autorizada deste arquivo é estritamente proibido
 * Proprietário e Confidencial
 * @link      https://github.com/incorpore/core-api para o repositório privado
 * @copyright Copyright (c) 2015 Incorpore Inc. (http://incorpore.com.br)
 * @author    Thiago R. Moreira <loganguns@gmail.com>
 */
 
namespace Core\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Credential
 *
 * @ORM\Table(name="credential")
 * @ORM\Entity
 */
class Credential
{

    /**
     * @var integer
     * @ORM\Column(name="credential_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $credentialId;

    /**
     * @var string
     * @ORM\Column(name="credential", type="string", length=60)
     */
    private $credential;

    /**
     * @var string
     * @ORM\Column(name="password", type="string", length=60)
     */
    private $password;

    /**
     * @var \Doctrine\Common\Collections\Collection
     * @ORM\ManyToMany(targetEntity="Core\Entity\User", mappedBy="credential")
     */
    private $user;

    /**
     * @var \Core\Entity\Role
     * @ORM\ManyToOne(targetEntity="Core\Entity\Role")
     * @ORM\JoinColumns({
     *      @ORM\JoinColumn(name="role_role_id", referencedColumnName="role_id")
     * })
     */
    private $role;

    /**
     * @var \Core\Entity\Enterprise
     * @ORM\ManyToOne(targetEntity="Core\Entity\Enterprise")
     * @ORM\JoinColumns({
     *      @ORM\JoinColumn(
     *          name="enterprise_enterprise_id",
     *          referencedColumnName="enterprise_id"
     *      )
     * })
     */
    private $enterprise;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->user = new ArrayCollection();
    }

    /**
     * Get credentialId
     *
     * @return integer
     */
    public function getCredentialId()
    {
        return $this->credentialId;
    }

    /**
     * Set credential
     *
     * @param string $credential            
     *
     * @return Role
     */
    public function setCredential($credential)
    {
        $this->credential = $credential;
        
        return $this;
    }

    /**
     * Get credential
     *
     * @return string
     */
    public function getCredential()
    {
        return $this->credential;
    }

    /**
     * Set password
     *
     * @param string $password            
     *
     * @return Role
     */
    public function setPassword($password)
    {
        $this->password = $password;
        
        return $this;
    }

    /**
     * Get password
     *
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Add user
     *
     * @param \Core\Entity\User $user            
     *
     * @return Credential
     */
    public function addUser(User $user)
    {
        $this->user[] = $user;
        
        return $this;
    }

    /**
     * Remove user
     *
     * @param \Core\Entity\User $user            
     */
    public function removeUser(User $user)
    {
        $this->user->removeElement($user);
    }

    /**
     * Get user
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Remove credential
     *
     * @param \Core\Entity\Credential $credential            
     */
    public function removeCredential(Credential $credential)
    {
        $this->credential->removeElement($credential);
    }

    /**
     * Set role
     *
     * @param \Core\Entity\Role $role            
     *
     * @return User
     */
    public function setRole(Role $role = null)
    {
        $this->role = $role;
        
        return $this;
    }

    /**
     * Get role
     *
     * @return \Core\Entity\Role
     */
    public function getRole()
    {
        return $this->role;
    }

    /**
     * Set enterprise
     *
     * @param \Core\Entity\Enterprise $enterprise            
     *
     * @return Enterprise
     */
    public function setEnterprise(Enterprise $enterprise = null)
    {
        $this->enterprise = $enterprise;
        
        return $this;
    }

    /**
     * Get enterprise
     *
     * @return \Core\Entity\Enterprise
     */
    public function getEnterprise()
    {
        return $this->enterprise;
    }
}
