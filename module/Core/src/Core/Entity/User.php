<?php
/**
 * Agille Park - Estacionamentos Inteligentes - Core-API
 * Copyright (C) Incorpore & WeDev Brasil - Todos os direitos reservados
 * Copia não autorizada deste arquivo é estritamente proibido
 * Proprietário e Confidencial
 * @link      https://github.com/incorpore/core-api para o repositório privado
 * @copyright Copyright (c) 2015 Incorpore Inc. (http://incorpore.com.br)
 * @author    Thiago R. Moreira <loganguns@gmail.com>
 */
 
namespace Core\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * User

 * @ORM\Table(
 *  name="user",
 *  indexes={
 *      @ORM\Index(
 *          name="fk_user_profile1_idx",
 *          columns={"profile_profile_id"}
 *      ),
 *      @ORM\Index(
 *          name="fk_user_role1_idx",
 *          columns={"role_role_id"}
 *      )
 *  }
 * )
 * @ORM\Entity
 */
class User
{

    /**
     * @var integer
     * @ORM\Column(name="user_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $userId;

    /**
     * @var \DateTime
     * @ORM\Column(name="create_date", type="datetime", nullable=true)
     */
    private $createDate;

    /**
     * @var \DateTime
     * @ORM\Column(name="last_update", type="datetime", nullable=true)
     */
    private $lastUpdate;

    /**
     * @var \Core\Entity\Profile
     * @ORM\ManyToOne(targetEntity="Core\Entity\Profile")
     * @ORM\JoinColumns({
     *  @ORM\JoinColumn(
     *      name="profile_profile_id",
     *      referencedColumnName="profile_id"
     *  )
     * })
     */
    private $profile;

    /**
     * @var string
     * @ORM\Column(name="doc_number", type="string", length=45, nullable=true)
     */
    private $docNumber;

    /**
     * @var \Doctrine\Common\Collections\Collection
     * @ORM\ManyToMany(targetEntity="Core\Entity\Enterprise", mappedBy="user")
     */
    private $enterprise;

    /**
     * @var \Doctrine\Common\Collections\Collection
     * @ORM\ManyToMany(targetEntity="Core\Entity\Car", inversedBy="user")
     * @ORM\JoinTable(
     *  name="user_has_car",
     *  joinColumns={
     *      @ORM\JoinColumn(name="user_user_id", referencedColumnName="user_id")
     *  },
     *  inverseJoinColumns={
     *      @ORM\JoinColumn(name="car_car_id", referencedColumnName="car_id")
     *  }
     * )
     */
    private $car;

    /**
     * @var \Doctrine\Common\Collections\Collection
     * @ORM\ManyToMany(targetEntity="Core\Entity\Contact", inversedBy="user")
     * @ORM\JoinTable(
     *  name="user_has_contact",
     *  joinColumns={
     *      @ORM\JoinColumn(
     *          name="user_user_id",
     *          referencedColumnName="user_id"
     *      )
     *  },
     *  inverseJoinColumns={
     *      @ORM\JoinColumn(
     *          name="contact_contact_id",
     *          referencedColumnName="contact_id"
     *      )
     *  }
     * )
     */
    private $contact;

    /**
     * @var \Doctrine\Common\Collections\Collection
     * @ORM\ManyToMany(targetEntity="Core\Entity\Credit", inversedBy="user")
     * @ORM\JoinTable(
     *  name="user_has_credit",
     *  joinColumns={
     *      @ORM\JoinColumn(name="user_user_id", referencedColumnName="user_id")
     *  },
     *  inverseJoinColumns={
     *      @ORM\JoinColumn(
     *          name="credit_credit_id",
     *          referencedColumnName="credit_id"
     *      )
     *  }
     * )
     */
    private $credit;

    /**
     * @var \Doctrine\Common\Collections\Collection
     * @ORM\ManyToMany(targetEntity="Core\Entity\Credential", inversedBy="user")
     * @ORM\JoinTable(
     *  name="user_has_credential",
     *  joinColumns={
     *      @ORM\JoinColumn(name="user_user_id", referencedColumnName="user_id")
     *  },
     *  inverseJoinColumns={
     *      @ORM\JoinColumn(
     *          name="credential_credential_id",
     *          referencedColumnName="credential_id"
     *      )
     *  }
     * )
     */
    private $credential;

    /**
     * @var \Core\Entity\Address
     * @ORM\ManyToOne(targetEntity="Core\Entity\Address", fetch="EAGER")
     * @ORM\JoinColumns({
     *  @ORM\JoinColumn(
     *      name="address_address_id",
     *      referencedColumnName="address_id"
     *  )
     * })
     */
    private $address;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->role = new ArrayCollection();
        $this->enterprise = new ArrayCollection();
        $this->car = new ArrayCollection();
        $this->contact = new ArrayCollection();
        $this->credit = new ArrayCollection();
        $this->credential = new ArrayCollection();
        $this->address = new ArrayCollection();
    }

    /**
     * Get userId
     *
     * @return integer
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * Set createDate
     *
     * @param \DateTime $createDate            
     *
     * @return User
     */
    public function setCreateDate($createDate)
    {
        $this->createDate = $createDate;
        
        return $this;
    }

    /**
     * Get createDate
     *
     * @return \DateTime
     */
    public function getCreateDate()
    {
        return $this->createDate;
    }

    /**
     * Set lastUpdate
     *
     * @param \DateTime $lastUpdate            
     *
     * @return User
     */
    public function setLastUpdate($lastUpdate)
    {
        $this->lastUpdate = $lastUpdate;
        
        return $this;
    }

    /**
     * Get lastUpdate
     *
     * @return \DateTime
     */
    public function getLastUpdate()
    {
        return $this->lastUpdate;
    }

    /**
     * Set profile
     *
     * @param \Core\Entity\Profile $profile            
     *
     * @return User
     */
    public function setProfile(Profile $profile = null)
    {
        $this->profile = $profile;
        
        return $this;
    }

    /**
     * Get profile
     *
     * @return \Core\Entity\Profile
     */
    public function getProfile()
    {
        return $this->profile;
    }

    /**
     * Set docNumber
     *
     * @param string $docNumber            
     *
     * @return Profile
     */
    public function setDocNumber($docNumber)
    {
        $this->docNumber = $docNumber;
        
        return $this;
    }

    /**
     * Get docNumber
     *
     * @return string
     */
    public function getDocNumber()
    {
        return $this->docNumber;
    }

    /**
     * Add enterprise
     *
     * @param \Core\Entity\Enterprise $enterprise            
     *
     * @return User
     */
    public function addEnterprise(Enterprise $enterprise)
    {
        $this->enterprise[] = $enterprise;
        
        return $this;
    }

    /**
     * Remove enterprise
     *
     * @param \Core\Entity\Enterprise $enterprise            
     */
    public function removeEnterprise(Enterprise $enterprise)
    {
        $this->enterprise->removeElement($enterprise);
    }

    /**
     * Get enterprise
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getEnterprise()
    {
        return $this->enterprise;
    }

    /**
     * Add car
     *
     * @param \Core\Entity\Car $car            
     *
     * @return User
     */
    public function addCar(Car $car)
    {
        $this->car[] = $car;
        
        return $this;
    }

    /**
     * Remove car
     *
     * @param \Core\Entity\Car $car            
     */
    public function removeCar(Car $car)
    {
        $this->car->removeElement($car);
    }

    /**
     * Get car
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCar()
    {
        return $this->car;
    }

    /**
     * Add contact
     *
     * @param \Core\Entity\Contact $contact            
     *
     * @return User
     */
    public function addContact(Contact $contact)
    {
        $this->contact[] = $contact;
        
        return $this;
    }

    /**
     * Remove contact
     *
     * @param \Core\Entity\Contact $contact            
     */
    public function removeContact(Contact $contact)
    {
        $this->contact->removeElement($contact);
    }

    /**
     * Get contact
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getContact()
    {
        return $this->contact;
    }

    /**
     * Add credit
     *
     * @param \Core\Entity\Credit $credit            
     *
     * @return User
     */
    public function addCredit(Credit $credit)
    {
        $this->credit[] = $credit;
        
        return $this;
    }

    /**
     * Remove credit
     *
     * @param \Core\Entity\Credit $credit            
     */
    public function removeCredit(Credit $credit)
    {
        $this->credit->removeElement($credit);
    }

    /**
     * Get credit
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCredit()
    {
        return $this->credit;
    }

    /**
     * Add credential
     *
     * @param \Core\Entity\Credential $credential            
     *
     * @return User
     */
    public function addCredential(Credential $credential)
    {
        $this->credential[] = $credential;
        
        return $this;
    }

    /**
     * Remove credential
     *
     * @param \Core\Entity\Credential $credential            
     */
    public function removeCredential(Credential $credential)
    {
        $this->credential->removeElement($credential);
    }

    /**
     * Get credential
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCredential()
    {
        return $this->credential;
    }

    /**
     * Set address
     *
     * @param \Core\Entity\Address $address            
     *
     * @return Profile
     */
    public function setAddress(Address $address = null)
    {
        $this->address = $address;
        
        return $this;
    }

    /**
     * Get address
     *
     * @return \Core\Entity\Address
     */
    public function getAddress()
    {
        return $this->address;
    }
}
