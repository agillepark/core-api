<?php
/**
 * Agille Park - Estacionamentos Inteligentes - Core-API
 * Copyright (C) Incorpore & WeDev Brasil - Todos os direitos reservados
 * Copia não autorizada deste arquivo é estritamente proibido
 * Proprietário e Confidencial
 * @link      https://github.com/incorpore/core-api para o repositório privado
 * @copyright Copyright (c) 2015 Incorpore Inc. (http://incorpore.com.br)
 * @author    Thiago R. Moreira <loganguns@gmail.com>
 */
 
namespace Core\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * RoleType
 *
 * @ORM\Table(name="role_type")
 * @ORM\Entity
 */
class RoleType
{

    /**
     * @var integer
     * @ORM\Column(name="role_type_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $roleTypeId;

    /**
     * Get roleTypeId
     *
     * @return integer
     */
    public function getRoleTypeId()
    {
        return $this->roleTypeId;
    }
}
