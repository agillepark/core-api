<?php
/**
 * Agille Park - Estacionamentos Inteligentes - Core-API
 * Copyright (C) Incorpore & WeDev Brasil - Todos os direitos reservados
 * Copia não autorizada deste arquivo é estritamente proibido
 * Proprietário e Confidencial
 * @link      https://github.com/incorpore/core-api para o repositório privado
 * @copyright Copyright (c) 2015 Incorpore Inc. (http://incorpore.com.br)
 * @author    Thiago R. Moreira <loganguns@gmail.com>
 */
 
namespace Core\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Contact

 * @ORM\Table(
 *  name="contact",
 *  indexes={
 *      @ORM\Index(name="fk_contact_contact_type1_idx",
 *      columns={"contact_type_contact_type_id"}
 *  )
 * })
 * @ORM\Entity
 */
class Contact
{

    /**
     * @var integer
     * @ORM\Column(name="contact_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $contactId;

    /**
     * @var string
     * @ORM\Column(name="value", type="string", length=45, nullable=true)
     */
    private $value;

    /**
     * @var \Core\Entity\ContactType
     * @ORM\ManyToOne(targetEntity="Core\Entity\ContactType")
     * @ORM\JoinColumns({
     *  @ORM\JoinColumn(
     *      name="contact_type_contact_type_id",
     *      referencedColumnName="contact_type_id"
     *  )
     * })
     */
    private $contactType;

    /**
     * @var \Doctrine\Common\Collections\Collection
     * @ORM\ManyToMany(
     *  targetEntity="Core\Entity\Enterprise",
     *  mappedBy="contact"
     * )
     */
    private $enterprise;

    /**
     * @var \Doctrine\Common\Collections\Collection
     * @ORM\ManyToMany(
     *  targetEntity="Core\Entity\Profile",
     *  mappedBy="contact"
     * )
     */
    private $profile;

    /**
     * @var \Doctrine\Common\Collections\Collection
     * @ORM\ManyToMany(
     *  targetEntity="Core\Entity\User",
     *  mappedBy="contact"
     * )
     */
    private $user;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->enterprise = new ArrayCollection();
        $this->profile = new ArrayCollection();
        $this->user = new ArrayCollection();
    }

    /**
     * Get contactId
     *
     * @return integer
     */
    public function getContactId()
    {
        return $this->contactId;
    }

    /**
     * Set value
     *
     * @param string $value            
     *
     * @return Contact
     */
    public function setValue($value)
    {
        $this->value = $value;
        
        return $this;
    }

    /**
     * Get value
     *
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Set contactType
     *
     * @param \Core\Entity\ContactType $contactType            
     *
     * @return Contact
     */
    public function setContactType(ContactType $contactType = null)
    {
        $this->contactType = $contactType;
        
        return $this;
    }

    /**
     * Get contactType
     *
     * @return \Core\Entity\ContactType
     */
    public function getContactType()
    {
        return $this->contactType;
    }

    /**
     * Add enterprise
     *
     * @param \Core\Entity\Enterprise $enterprise            
     *
     * @return Contact
     */
    public function addEnterprise(Enterprise $enterprise)
    {
        $this->enterprise[] = $enterprise;
        
        return $this;
    }

    /**
     * Remove enterprise
     *
     * @param \Core\Entity\Enterprise $enterprise            
     */
    public function removeEnterprise(Enterprise $enterprise)
    {
        $this->enterprise->removeElement($enterprise);
    }

    /**
     * Get enterprise
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getEnterprise()
    {
        return $this->enterprise;
    }

    /**
     * Add profile
     *
     * @param \Core\Entity\Profile $profile            
     *
     * @return Contact
     */
    public function addProfileProfile(Profile $profile)
    {
        $this->profile[] = $profile;
        
        return $this;
    }

    /**
     * Remove profile
     *
     * @param \Core\Entity\Profile $profile            
     */
    public function removeProfileProfile(Profile $profile)
    {
        $this->profile->removeElement($profile);
    }

    /**
     * Get profile
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getProfileProfile()
    {
        return $this->profile;
    }

    /**
     * Add user
     *
     * @param \Core\Entity\User $user            
     *
     * @return Contact
     */
    public function addUser(User $user)
    {
        $this->user[] = $user;
        
        return $this;
    }

    /**
     * Remove user
     *
     * @param \Core\Entity\User $user            
     */
    public function removeUser(User $user)
    {
        $this->user->removeElement($user);
    }

    /**
     * Get user
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getUser()
    {
        return $this->user;
    }
}
