<?php
/**
 * Agille Park - Estacionamentos Inteligentes - Core-API
 * Copyright (C) Incorpore & WeDev Brasil - Todos os direitos reservados
 * Copia não autorizada deste arquivo é estritamente proibido
 * Proprietário e Confidencial
 * @link      https://github.com/incorpore/core-api para o repositório privado
 * @copyright Copyright (c) 2015 Incorpore Inc. (http://incorpore.com.br)
 * @author    Thiago R. Moreira <loganguns@gmail.com>
 */
 
namespace Core\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Credit

 * @ORM\Table(
 *  name="credit",
 *  indexes={@ORM\Index(
 *      name="fk_credit_city1_idx",
 *      columns={"city_city_id"}
 *  )}
 * )
 * @ORM\Entity
 */
class Credit
{

    /**
     * @var integer
     * @ORM\Column(name="credit_id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $creditId;

    /**
     * @var string
     * @ORM\Column(name="value", type="string", length=45, nullable=true)
     */
    private $value;

    /**
     * @var \Core\Entity\City
     * @ORM\ManyToOne(targetEntity="Core\Entity\City")
     * @ORM\JoinColumns({
     *  @ORM\JoinColumn(name="city_city_id", referencedColumnName="city_id")
     * })
     */
    private $city;

    /**
     * @var \Doctrine\Common\Collections\Collection
     * @ORM\ManyToMany(
     *  targetEntity="Core\Entity\User",
     *  inversedBy="credit"
     * )
     * @ORM\JoinTable(
     *  name="user_has_credit",
     *  joinColumns={
     *      @ORM\JoinColumn(
     *          name="credit_credit_id",
     *          referencedColumnName="credit_id"
     *      )
     *  },
     *  inverseJoinColumns={
     *      @ORM\JoinColumn(
     *          name="user_user_id",
     *          referencedColumnName="user_id"
     *      )
     *  }
     * )
     */
    private $user;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->user = new ArrayCollection();
    }

    /**
     * Get creditId
     *
     * @return integer
     */
    public function getCreditId()
    {
        return $this->creditId;
    }

    /**
     * Set value
     *
     * @param string $value            
     *
     * @return Credit
     */
    public function setValue($value)
    {
        $this->value = $value;
        
        return $this;
    }

    /**
     * Get value
     *
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Set city
     *
     * @param \Core\Entity\City $city            
     *
     * @return Credit
     */
    public function setCity(City $city = null)
    {
        $this->city = $city;
        
        return $this;
    }

    /**
     * Get city
     *
     * @return \Core\Entity\City
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Add user
     *
     * @param \Core\Entity\User $user            
     *
     * @return Credit
     */
    public function addUser(User $user)
    {
        $this->user[] = $user;
        
        return $this;
    }

    /**
     * Remove user
     *
     * @param \Core\Entity\User $user            
     */
    public function removeUser(User $user)
    {
        $this->user->removeElement($user);
    }

    /**
     * Get user
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getUser()
    {
        return $this->user;
    }
}
