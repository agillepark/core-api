<?php
/**
 * Agille Park - Estacionamentos Inteligentes - Core-API
 * Copyright (C) Incorpore & WeDev Brasil - Todos os direitos reservados
 * Copia não autorizada deste arquivo é estritamente proibido
 * Proprietário e Confidencial
 * @link      https://github.com/incorpore/core-api para o repositório privado
 * @copyright Copyright (c) 2015 Incorpore Inc. (http://incorpore.com.br)
 * @author    Thiago R. Moreira <loganguns@gmail.com>
 */
 
namespace Core\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Car
 *
 * @ORM\Table(name="car")
 * @ORM\Entity
 */
class Car
{

    /**
     * @var integer
     * @ORM\Column(
     *  name="car_id",
     *  type="integer",
     *  nullable=false
     * )
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $carId;

    /**
     * @var string @ORM\Column(
     *  name="license_plate",
     *  type="string",
     *  length=45,
     *  nullable=true
     * )
     */
    private $licensePlate;

    /**
     * @var \Doctrine\Common\Collections\Collection
     * @ORM\ManyToMany(
     * targetEntity="Core\Entity\User",
     *  mappedBy="car"
     * )
     */
    private $user;

    /**
     * Constructor
     */
    public function __construct()
    {
        /** @noinspection PhpUnnecessaryFullyQualifiedNameInspection */
        $this->user = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get carId
     *
     * @return integer
     */
    public function getCarId()
    {
        return $this->carId;
    }

    /**
     * Set licensePlate
     *
     * @param string $licensePlate            
     *
     * @return Car
     */
    public function setLicensePlate($licensePlate)
    {
        $this->licensePlate = $licensePlate;
        
        return $this;
    }

    /**
     * Get licensePlate
     *
     * @return string
     */
    public function getLicensePlate()
    {
        return $this->licensePlate;
    }

    /**
     * Add user
     *
     * @param \Core\Entity\User $user            
     *
     * @return Car
     */
    public function addUser(User $user)
    {
        $this->user[] = $user;
        
        return $this;
    }

    /**
     * Remove user
     *
     * @param \Core\Entity\User $user            
     */
    public function removeUser(User $user)
    {
        $this->user->removeElement($user);
    }

    /**
     * Get user
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getUser()
    {
        return $this->user;
    }
}
