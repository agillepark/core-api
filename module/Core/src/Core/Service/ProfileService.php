<?php
/**
 * Agille Park - Estacionamentos Inteligentes - Core-API
 * Copyright (C) Incorpore & WeDev Brasil - Todos os direitos reservados
 * Copia não autorizada deste arquivo é estritamente proibido
 * Proprietário e Confidencial
 * @link      https://github.com/incorpore/core-api para o repositório privado
 * @copyright Copyright (c) 2015 Incorpore Inc. (http://incorpore.com.br)
 * @author    Thiago R. Moreira <loganguns@gmail.com>
 */

namespace Core\Service;

use Core\Service\AbstractService\AbstractService;

/**
 * Profile Service
 * @category Core
 * @package  Service
 * @author   Thiago R. Moreira <loganguns@gmail.com>
 */
class ProfileService extends AbstractService
{

    /**
     * @return mixed
     */
    public function getList()
    {
        $this->queryBuilder()->select('p')
            ->from('Core\Entity\Profile', 'p')
            ->orderBy('p.profileId', 'ASC')
            ->setMaxResults(20)
            ->setFirstResult(0);
        /*
        if (! empty($this->params()->fromQuery('enterprise'))) {
            
            /*
             * $queryBuilder->leftJoin('u.enterprise', 'e')
             * ->where('e.enterpriseId = :enterprise')
             * ->setParameter('enterprise', $this->params()->fromQuery('enterprise'));
             
        }
        */
        /** @noinspection PhpUnnecessaryFullyQualifiedNameInspection */
        $results = $this->queryBuilder()->getQuery()->getResult(
            \Doctrine\ORM\AbstractQuery::HYDRATE_ARRAY
        );
        
        return $results;
    }

    /**
     * Retorna um inividuo do recurso solicitado
     *
     * @param mixed $profileId
     *
     * @return mixed
     * @internal param mixed $id
     */
    public function get($profileId)
    {
        $this->queryBuilder()->select('p')
            ->where('p.profileId =' . $profileId)
            ->from('Core\Entity\Profile', 'p');
        
        return $this;
    }

    /**
     * Cria um novo recurso
     *
     * @param  mixed $data
     *
     * @return mixed
     */
    public function create($data)
    {
        return new JsonModel(
            [
                'data' => "Create Profile"
            ]
        );
    }

    /**
     * Atualiza um recurso exixtente
     *
     * @param mixed  $profileId
     * @param  mixed $data
     * @return mixed
     * @internal param mixed $id
     */
    public function update($profileId, $data)
    {
        $crypt = new CryptResponse();
        
        foreach ($data as $key => $curr) {
            $data[$key] = $crypt->decrypt($curr);
        }
        
        $qb = $this->getEntityManager()->createQueryBuilder();
        $q = $qb->update('Core\Entity\Profile', 'u')
            ->set('p.docNumber', '?1')
            ->where('u.profileId = ?2')
            ->setParameter(1, $data['docNumber'])
            ->setParameter(2, $profileId)
            ->getQuery();

        $q->execute();
        
        /*
         * $data = new Profile();
         * $data->setId(123);
         * $data->setName('test');
         *
         * $entityManager->merge($data);
         * $entityManager->flush();
         */
        
        return $this->serialize(
            [
                $data
            ],
            false
        );
        // return $this->serialize(null);
    }

    /**
     * @param AbstractService\unknown $profileId
     * @return bool
     * @internal param unknown $id
     *
     */
    public function delete($profileId)
    {
        return new JsonModel(
            [
                'data' => "Delete Profile"
            ]
        );
    }

    /**
     * @return JsonModel
     */
    public function options()
    {
        return new JsonModel(
            [
                'data' => "Methods Allowed"
            ]
        );
    }
}
