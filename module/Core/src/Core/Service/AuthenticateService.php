<?php
/**
 * Agille Park - Estacionamentos Inteligentes - Core-API
 * Copyright (C) Incorpore & WeDev Brasil - Todos os direitos reservados
 * Copia não autorizada deste arquivo é estritamente proibido
 * Proprietário e Confidencial
 * @link      https://github.com/incorpore/core-api para o repositório privado
 * @copyright Copyright (c) 2015 Incorpore Inc. (http://incorpore.com.br)
 * @author    Thiago R. Moreira <loganguns@gmail.com>
 */

namespace Core\Service;

use Core\Service\AbstractService\AbstractService;
use Doctrine\ORM\AbstractQuery;
use Zend\Crypt\Password\Bcrypt;

/**
 * Serviço de autenticação de Usuários
 * @category Core
 * @package  Service
 * @author   Thiago R. Moreira <loganguns@gmail.com>
 */
class AuthenticateService extends AbstractService
{

    /* (non-PHPdoc)
     * @see \Core\Service\AbstractService\AbstractService::getList()
     */
    /**
     * @return $this
     */
    public function getList()
    {
        $this->queryBuilder()->select('c', 'p', 'r', 'u')
            ->from('Core\Entity\Credential', 'c')
            ->leftJoin('c.user', 'u')
            ->leftJoin('u.profile', 'p')
            ->leftJoin('u.role', 'r')
            ->orderBy('c.credentialId', 'ASC')
            ->setMaxResults(20)
            ->setFirstResult(0);
        
        return $this;
    }

    /* (non-PHPdoc)
     * @see \Core\Service\AbstractService\AbstractService::get()
     */
    /**
     * Retorna um inividuo do recurso solicitado
     *
     * @param mixed $credentialId
     *
     * @return mixed
     * @internal param mixed $id
     */
    public function get($credentialId)
    {
        $this->queryBuilder()->select('c', 'p', 'r')
            ->where('u.credentialId =' . $credentialId)
            ->from('Core\Entity\Credential', 'c')
            ->leftJoin('u.profile', 'p')
            ->leftJoin('u.role', 'r');
        // ->leftJoin('u.address', 'a')
        
        return $this;
    }

    /* (non-PHPdoc)
     * @see \Core\Service\AbstractService\AbstractService::create()
     */
    /**
     * Cria um novo recurso
     *
     * @param  mixed $data
     *
     * @return mixed
     */
    public function create($data) // Authenticate
    {
        // Login
        $this->queryBuilder()->select('c', 'r')
            ->from('Core\Entity\Credential', 'c')
            ->leftJoin('c.role', 'r')
            //->leftJoin('c.user', 'u')
            //->leftJoin('u.profile', 'p')
            ->where('c.credential = :credential')
            ->setParameter('credential', $data['credential']);
        // ->leftJoin('u.address', 'a')
        
        $results = $this->queryBuilder()->getQuery()->getResult(
            AbstractQuery::HYDRATE_ARRAY
        );
        
        // To Extract Form Array
        $credential = $results['0'];
        
        $bcrypt = new Bcrypt();
        
        if ($bcrypt->verify($data['password'], $credential['password'])) {
            
            // Senha Correta
            $login = [
                'login' => true
            ];
            $credential = [
                'credential' => $credential
            ];
            
            $output = array_merge($login, $credential);
        } else {
            // Credenciais Invalida
            $output = [
                'login' => false,
                'errorCode' => 1
            ];
        }
        // Fim Login
        
        return [$output];
    }

    /* (non-PHPdoc)
     * @see \Core\Service\AbstractService\AbstractService::update()
     */
    /**
     * Atualiza um recurso exixtente
     *
     * @param mixed  $credentialId
     * @param  mixed $data
     * @return mixed
     * @internal param mixed $id
     */
    public function update($credentialId, $data)
    {
        return new JsonModel(
            [
                'data' => "Update Credential"
            ]
        );
    }

    /* (non-PHPdoc)
     * @see \Core\Service\AbstractService\AbstractService::delete()
     */
    /**
     * @param AbstractService\unknown $credentialId
     * @return bool
     * @internal param unknown $id
     *
     */
    public function delete($credentialId)
    {
        return new JsonModel(
            [
                'data' => "Delete Credential"
            ]
        );
    }

    /**
     * @return \Core\Service\JsonModel
     */
    public function options()
    {
        return new JsonModel(
            [
                'data' => "Methods Allowed"
            ]
        );
    }
}
