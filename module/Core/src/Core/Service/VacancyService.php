<?php
/**
 * Agille Park - Estacionamentos Inteligentes - Core-API
 * 
 * Copyright (C) Incorpore & WeDev Brasil - Todos os direitos reservados
 * Copia não autorizada deste arquivo é estritamente proibido
 * Proprietário e Confidencial
 *
 * @link      https://github.com/incorpore/core-api para o repositório privado
 * @copyright Copyright (c) 2015 Incorpore Inc. (http://incorpore.com.br)
 * @author   Thiago R. Moreira <loganguns@gmail.com>
 */
 
namespace Core\Service;

use Core\Service\AbstractService\AbstractService;

/**
 * Vacancy Service
 *
 * @category Core
 * @package  Service
 * @author   Thiago R. Moreira <loganguns@gmail.com>
 */
class VacancyService extends AbstractService
{

    /**
     * @see \Core\Service\AbstractService\AbstractService::getList()
     */
    public function getList()
    {
        $this->queryBuilder()->select('v')
            ->from('Core\Entity\Vacancy', 'v')
            ->orderBy('v.vacancyId', 'ASC');
        /*
        if (! empty($this->params()->fromQuery('city'))) {
            
            $queryBuilder->leftJoin('v.city', 'c')
                ->where('c.cityId = :city')
                ->setParameter('city', $this->params()->fromQuery('city'));
        }
        
        if ($this->params()->fromQuery('count')) {
            
            $queryBuilder->select('count(v.vacancyId) as total');
        }
        */
        /*
        $results = $queryBuilder->getQuery()->getResult(
            \Doctrine\ORM\AbstractQuery::HYDRATE_ARRAY
        );
        */
        //return $queryBuilder;
        
        return $this;
    }

    /**
     * @see \Core\Service\AbstractService\AbstractService::get()
     *
     * @param mixed $vacancyId
     *
     * @return $this|mixed
     */
    public function get($vacancyId)
    {
        $this->queryBuilder()->select('v', 'vt', 'c')
            ->from('Core\Entity\Vacancy', 'v')
            ->join('v.city', 'c')
            ->join('v.vacancyType', 'vt')
            ->orderBy('v.vacancyId', 'ASC')
            ->where('v.vacancyId = :vacancyId')
            ->setParameter('vacancyId', $vacancyId);
        
        return $this;
    }

    /**
     * @see \Core\Service\AbstractService\AbstractService::create()
     * @param mixed $data
     * @return JsonModel|mixed
*/
    public function create($data)
    {
        return new JsonModel(
            [
                'data' => "Create Vacancy"
            ]
        );
    }

    /**
     * @see \Core\Service\AbstractService\AbstractService::update()
     * @param mixed $vacancyId
     * @param mixed $data
     * @return JsonModel|mixed
*/
    public function update($vacancyId, $data)
    {
        return new JsonModel(
            [
                'data' => "Update Vacancy"
            ]
        );
    }

    /**
     * @see \Core\Service\AbstractService\AbstractService::delete()
     * @param AbstractService\unknown $vacancyId
     * @return bool|JsonModel
*/
    public function delete($vacancyId)
    {
        return new JsonModel(
            [
                'data' => "Delete Vacancy"
            ]
        );
    }

    /**
     * @see \Core\Service\AbstractService\AbstractService::options()
     */
    public function options()
    {
        return new JsonModel(
            [
                'data' => "Methods Allowed"
            ]
        );
    }
}
