<?php
/**
 * Agille Park - Estacionamentos Inteligentes - Core-API
 * Copyright (C) Incorpore & WeDev Brasil - Todos os direitos reservados
 * Copia não autorizada deste arquivo é estritamente proibido
 * Proprietário e Confidencial
 * @link      https://github.com/incorpore/core-api para o repositório privado
 * @copyright Copyright (c) 2015 Incorpore Inc. (http://incorpore.com.br)
 * @author    Thiago R. Moreira <loganguns@gmail.com>
 */

namespace Core\Service;

use Core\Service\AbstractService\AbstractService;

/**
 * Sector Service
 * @category Core
 * @package  Service
 * @author   Thiago R. Moreira <loganguns@gmail.com>
 */
class SectorService extends AbstractService
{

    /**
     * @return void
     */
    public function getList()
    {
        $this->queryBuilder()->select('s')
            ->from('Core\Entity\Sector', 's')
            ->orderBy('s.sectorId', 'ASC')
            ->setMaxResults(20)
            ->setFirstResult(0);

        if (! empty($this->params()->fromQuery('city'))) {

            $this->queryBuilder()->leftJoin('s.city', 'c')
                ->where('c.cityId = :city')
                ->setParameter('city', $this->params()->fromQuery('city'));
        }
        
        if ($this->params()->fromQuery('count')) {

            //$this->queryBuilder()->select('count(s.sectorId) as total');
        }

        return $this;
    }

    /**
     * @param unknown $sectorId
     *
     * @return unknown
     */
    public function get($sectorId)
    {
        $this->queryBuilder()->select('s', 'c')
            ->from('Core\Entity\Sector', 's')
            ->join('s.city', 'c')
            ->orderBy('s.sectorId', 'ASC')
            ->where('s.sectorId = :sectorId')
            ->setParameter('sectorId', $sectorId);
        
        return $this;
    }

    /**
     * @param unknown $data
     *
     * @return \Core\Service\JsonModel
     */
    public function create($data)
    {
        return new JsonModel(
            [
                'data' => "Create Sector"
            ]
        );
    }

    /**
     * @param unknown $sectorId
     * @param unknown $data
     *
     * @return \Core\Service\JsonModel
     */
    public function update($sectorId, $data)
    {
        return new JsonModel(
            [
                'data' => "Update Sector"
            ]
        );
    }

    /**
     * @param unknown $sectorId
     *
     * @return \Core\Service\JsonModel
     */
    public function delete($sectorId)
    {
        return new JsonModel(
            [
                'data' => "Delete Sector"
            ]
        );
    }

    /**
     * @return \Core\Service\JsonModel
     */
    public function options()
    {
        return new JsonModel(
            [
                'data' => "Methods Allowed"
            ]
        );
    }
}
