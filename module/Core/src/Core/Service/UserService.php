<?php
/**
 * Agille Park - Estacionamentos Inteligentes - Core-API
 * Copyright (C) Incorpore & WeDev Brasil - Todos os direitos reservados
 * Copia não autorizada deste arquivo é estritamente proibido
 * Proprietário e Confidencial
 * @link      https://github.com/incorpore/core-api para o repositório privado
 * @copyright Copyright (c) 2015 Incorpore Inc. (http://incorpore.com.br)
 * @author    Thiago R. Moreira <loganguns@gmail.com>
 */

namespace Core\Service;

use Core\Service\AbstractService\AbstractService;

/**
 * User Service
 *
 * @category Core
 * @package  Service
 * @author   Thiago R. Moreira <loganguns@gmail.com>
 */
class UserService extends AbstractService
{

    /**
     * @see \Core\Service\AbstractService\AbstractService::getList()
     */
    public function getList()
    {
        $this->queryBuilder()->select('u', 'p')
            ->from('Core\Entity\User', 'u')
            ->leftJoin('u.profile', 'p')
            ->orderBy('u.userId', 'ASC');

        return $this;
    }

    /**
     * @see \Core\Service\AbstractService\AbstractService::get()
     *
     * @param mixed $userId
     *
     * @return $this|mixed
     */
    public function get($userId)
    {
        $this->queryBuilder()->select('u', 'p')
            ->where('u.userId =' . $userId)
            ->from('Core\Entity\User', 'u')
            ->leftJoin('u.profile', 'p');
        //   ->leftJoin('u.address', 'a')

        return $this;
    }

    /**
     * @see \Core\Service\AbstractService\AbstractService::create()
     * @param mixed $data
     * @return JsonModel|mixed
*/
    public function create($data)
    {
        return new JsonModel(
            [
                'data' => "Create User"
            ]
        );
    }

    /**
     * @see \Core\Service\AbstractService\AbstractService::update()
     * @param mixed $userId
     * @param mixed $data
     * @return mixed
*/
    public function update($userId, $data)
    {
        $crypt = new CryptResponse();
        
        foreach ($data as $key => $curr) {
            $data[$key] = $crypt->decrypt($curr);
        }
        
        $qb = $this->getEntityManager()->createQueryBuilder();
        $q = $qb->update('Core\Entity\User', 'u')
            ->set('u.docNumber', '?1')
            ->where('u.userId = ?2')
            ->setParameter(1, $data['docNumber'])
            ->setParameter(2, $userId)
            ->getQuery();
        $q->execute();

        /*
         * $data = new User();
         * $data->setId(123);
         * $data->setName('test');
         *
         * $entityManager->merge($data);
         * $entityManager->flush();
         */
        
        // return $this->serialize(array($data), false);
        return $this->serialize(null);
    }

    /**
     * @see \Core\Service\AbstractService\AbstractService::delete()
     * @param AbstractService\unknown $userId
     * @return bool|JsonModel
*/
    public function delete($userId)
    {
        return new JsonModel(
            [
                'data' => "Delete User"
            ]
        );
    }

    /**
     * @return \Core\Service\JsonModel
     */
    public function options()
    {
        return new JsonModel(
            [
                'data' => "Methods Allowed"
            ]
        );
    }
}
