<?php
/**
 * Agille Park - Estacionamentos Inteligentes - Core-API
 * Copyright (C) Incorpore & WeDev Brasil - Todos os direitos reservados
 * Copia não autorizada deste arquivo é estritamente proibido
 * Proprietário e Confidencial
 * @link      https://github.com/incorpore/core-api para o repositório privado
 * @copyright Copyright (c) 2015 Incorpore Inc. (http://incorpore.com.br)
 * @author    Thiago R. Moreira <loganguns@gmail.com>
 */

namespace Core\Service;

use Core\Service\AbstractService\AbstractService;

/**
 * Credit Service
 * @category Core
 * @package  Service
 * @author   Thiago R. Moreira <loganguns@gmail.com>
 */
class CreditService extends AbstractService
{

    /**
     * @see \Core\Service\AbstractService\AbstractService::getList()
     */
    public function getList()
    {
        $this->queryBuilder()->select('c', 'ci')
            ->from('Core\Entity\Credit', 'c')
            ->leftjoin('c.city', 'ci')
            ->orderBy('c.creditId', 'ASC')
            ->setMaxResults(20)
            ->setFirstResult(0);
        /*
        if ( !empty($this->params()->fromQuery('user'))) {
            
            $queryBuilder->leftJoin('c.user', 'u')
                ->where('u.userId = :user')
                ->setParameter('user', $this->params()->fromQuery('user'));
        }
        */
        return $this;
    }

    /**
     * @see \Core\Service\AbstractService\AbstractService::get()
     *
     * @param mixed $creditId
     *
     * @return $this|mixed
     */
    public function get($creditId)
    {
        $this->queryBuilder()->select('c')
            ->from('Core\Entity\Credit', 'c')
            ->orderBy('c.creditId', 'ASC')
            ->where('c.creditId = :creditId')
            ->setParameter('creditId', $creditId);
        
        return $this;
    }

    /**
     * @see \Core\Service\AbstractService\AbstractService::create()
     * @param mixed $data
     * @return JsonModel|mixed
*/
    public function create($data)
    {
        return new JsonModel(
            [
                'data' => "Create Credit"
            ]
        );
    }

    /**
     * @see \Core\Service\AbstractService\AbstractService::update()
     * @param mixed $creditId
     * @param mixed $data
     * @return JsonModel|mixed
*/
    public function update($creditId, $data)
    {
        return new JsonModel(
            [
                'data' => "Update Credit"
            ]
        );
    }

    /**
     * @see \Core\Service\AbstractService\AbstractService::delete()
     * @param AbstractService\unknown $creditId
     * @return bool|JsonModel
*/
    public function delete($creditId)
    {
        return new JsonModel(
            [
                'data' => "Delete Credit"
            ]
        );
    }

    /**
     * @see \Core\Service\AbstractService\AbstractService::options()
     */
    public function options()
    {
        return new JsonModel(
            [
                'data' => "Methods Allowed"
            ]
        );
    }
}
