<?php
/**
 * Agille Park - Estacionamentos Inteligentes - Core-API
 * Copyright (C) Incorpore & WeDev Brasil - Todos os direitos reservados
 * Copia não autorizada deste arquivo é estritamente proibido
 * Proprietário e Confidencial
 * @link      https://github.com/incorpore/core-api para o repositório privado
 * @copyright Copyright (c) 2015 Incorpore Inc. (http://incorpore.com.br)
 * @author    Thiago R. Moreira <loganguns@gmail.com>
 */

namespace Core\Service;

use Core\Service\AbstractService\AbstractService;

/**
 * Credential Service
 * @category Core
 * @package  Service
 * @author   Thiago R. Moreira <loganguns@gmail.com>
 */
class CredentialService extends AbstractService
{

    /**
     *
     */
    public function getList()
    {
        $this->queryBuilder()->select('c', 'e')
            ->from('Core\Entity\Credential', 'c')
            ->join('c.enterprise', 'e')
            ->orderBy('c.credentialId', 'ASC')
            ->setMaxResults(20)
            ->setFirstResult(0);
        
        return $this;
    }

    /**
     * Retorna um inividuo do recurso solicitado
     *
     * @param mixed $credentialId
     *
     * @return mixed
     * @internal param mixed $id
     */
    public function get($credentialId)
    {
        $this->queryBuilder()->select('c', 'e', 'u', 'p')
            ->from('Core\Entity\Credential', 'c')
            ->leftJoin('c.enterprise', 'e')
            ->leftJoin('c.user', 'u')
            ->leftJoin('u.profile', 'p')
            ->orderBy('c.credentialId', 'ASC')
            ->where('c.credentialId = :credentialId')
            ->setParameter('credentialId', $credentialId);
        
        return $this;
    }

    /**
     * Cria um novo recurso
     *
     * @param  mixed $data
     *
     * @return mixed
     */
    public function create($data)
    {
        return new JsonModel(
            [
                'data' => "Create Credential"
            ]
        );
    }

    /**
     * Atualiza um recurso exixtente
     *
     * @param mixed  $credentialid
     * @param  mixed $data
     * @return mixed
     * @internal param mixed $id
*/
    public function update($credentialid, $data)
    {
        return new JsonModel(
            [
                'data' => "Update Credential"
            ]
        );
    }

    /**
     * @param AbstractService\unknown $credentialId
     * @return bool
     * @internal param unknown $id
     *
*/
    public function delete($credentialId)
    {
        return new JsonModel(
            [
                'data' => "Delete Credential"
            ]
        );
    }

    /**
     * @return JsonModel
     */
    public function options()
    {
        return new JsonModel(
            [
                'data' => "Methods Allowed"
            ]
        );
    }
}
