<?php
/**
 * Agille Park - Estacionamentos Inteligentes - Core-API
 * Copyright (C) Incorpore & WeDev Brasil - Todos os direitos reservados
 * Copia não autorizada deste arquivo é estritamente proibido
 * Proprietário e Confidencial
 * @link      https://github.com/incorpore/core-api para o repositório privado
 * @copyright Copyright (c) 2015 Incorpore Inc. (http://incorpore.com.br)
 * @author    Thiago R. Moreira <loganguns@gmail.com>
 */

namespace Core\Service;

/**
 * Index Service
 * @category Core
 * @package  Service
 * @author   Thiago R. Moreira <loganguns@gmail.com>
 */
class IndexService
{

    /**
     * @see \Core\Service\AbstractService\AbstractService::getList()
     */
    public function getList()
    {
        return $this;
    }
    
    /**
     * @see \Core\Service\AbstractService\AbstractService::get()
     */
    public function get()
    {
        return $this;
    }
    
    /**
     * @see \Core\Service\AbstractService\AbstractService::create()
     */
    public function create()
    {
        return $this;
    }
    
    /**
     * @see \Core\Service\AbstractService\AbstractService::update()
     */
    public function update()
    {
        return $this;
    }
    
    /**
     * @see \Core\Service\AbstractService\AbstractService::delete()
     */
    public function delete()
    {
        return $this;
    }
    
    /**
     * @return \Core\Service\JsonModel
     */
    public function options()
    {
        return $this;
    }
    
    /**
     * @return array
     */
    public function getResult()
    {
        return ["Welcome to iPark API"];
    }
}
