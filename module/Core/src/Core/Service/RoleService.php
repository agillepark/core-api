<?php
/**
 * Agille Park - Estacionamentos Inteligentes - Core-API
 * Copyright (C) Incorpore & WeDev Brasil - Todos os direitos reservados
 * Copia não autorizada deste arquivo é estritamente proibido
 * Proprietário e Confidencial
 * @link      https://github.com/incorpore/core-api para o repositório privado
 * @copyright Copyright (c) 2015 Incorpore Inc. (http://incorpore.com.br)
 * @author    Thiago R. Moreira <loganguns@gmail.com>
 */

namespace Core\Service;

use Core\Service\AbstractService\AbstractService;

/**
 * Role Service
 * @category Core
 * @package  Service
 * @author   Thiago R. Moreira <loganguns@gmail.com>
 */
class RoleService extends AbstractService
{

    /**
     *
     */
    public function getList()
    {
        $this->queryBuilder()->select('r')
            ->from('Core\Entity\Role', 'r')
            ->setMaxResults(20)
            ->setFirstResult(0);
        /*
        if (! empty($this->params()->fromQuery('user'))) {
            
            $queryBuilder->where('u.roleId = :roleId')
            ->setParameter('roleId', $this->params()->fromQuery('user'));
        }
        */
        return $this;
    }

    /**
     * Retorna um inividuo do recurso solicitado
     *
     * @param mixed $roleId
     *
     * @return mixed
     * @internal param mixed $id
     */
    public function get($roleId)
    {
        $this->queryBuilder()->select('r')
            ->where('r.roleId =' . $roleId)
            ->from('Core\Entity\Role', 'r');
        
        return $this;
    }

    /**
     * Cria um novo recurso
     *
     * @param  mixed $data
     *
     * @return mixed
     */
    public function create($data)
    {
        return ["Create Role"];
    }

    /**
     * Atualiza um recurso exixtente
     *
     * @param mixed  $roleId
     * @param  mixed $data
     * @return mixed
     * @internal param mixed $id
     */
    public function update($roleId, $data)
    {
        return new JsonModel(
            [
                'data' => "Update User"
            ]
        );
    }

    /**
     * @param AbstractService\unknown $roleId
     * @return bool
     * @internal param unknown $id
     *
     */
    public function delete($roleId)
    {
        return new JsonModel(
            [
                'data' => "Delete User"
            ]
        );
    }

    /**
     * @return JsonModel
     */
    public function options()
    {
        return new JsonModel(
            [
                'data' => "Methods Allowed"
            ]
        );
    }
}
