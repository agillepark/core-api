<?php
/**
 * Agille Park - Estacionamentos Inteligentes - Core-API
 * Copyright (C) Incorpore & WeDev Brasil - Todos os direitos reservados
 * Copia não autorizada deste arquivo é estritamente proibido
 * Proprietário e Confidencial
 * @link      https://github.com/incorpore/core-api para o repositório privado
 * @copyright Copyright (c) 2015 Incorpore Inc. (http://incorpore.com.br)
 * @author    Thiago R. Moreira <loganguns@gmail.com>
 */

namespace Core\Service;

use Core\Service\AbstractService\AbstractService;

/**
 * Contact Service
 * @category Core
 * @package  Service
 * @author   Thiago R. Moreira <loganguns@gmail.com>
 */
class ContactService extends AbstractService
{
    /**
     * @see \Core\Service\AbstractService\AbstractService::getList()
     */
    public function getList()
    {
        $this->queryBuilder()->select('c', 't')
            ->from('Core\Entity\Contact', 'c')
            ->leftJoin('c.contactType', 't')
            ->orderBy('c.contactId', 'ASC')
            ->setMaxResults(20)
            ->setFirstResult(0);
        /*
        if ( $this->queryParams != null) {
            
            $queryBuilder->leftJoin('c.user', 'u')
                ->where('u.userId = :user')
                ->setParameter('user', $this->params()->fromQuery('user'));
                
        }
        
        $results= $queryBuilder->getQuery()->getResult(
            \Doctrine\ORM\AbstractQuery::HYDRATE_ARRAY
        );
        */
        return $this;
    }

    /**
     * @see \Core\Service\AbstractService\AbstractService::get()
     *
     * @param mixed $contactId
     *
     * @return $this|mixed
     */
    public function get($contactId)
    {
        $this->queryBuilder()->select('c', 't')
            ->from('Core\Entity\Contact', 'c')
            ->leftJoin('c.contactType', 't')
            ->orderBy('c.contactId', 'ASC')
            ->where('c.contactId = :contactId')
            ->setParameter('contactId', $contactId);
        
        return $this;
    }

    /**
     * @see \Core\Service\AbstractService\AbstractService::create()
     * @param mixed $data
     * @return JsonModel|mixed
*/
    public function create($data)
    {
        return new JsonModel(
            [
                'data' => "Create Contact"
            ]
        );
    }

    /**
     * @see \Core\Service\AbstractService\AbstractService::update()
     * @param mixed $contactId
     * @param mixed $data
     * @return JsonModel|mixed
*/
    public function update($contactId, $data)
    {
        return new JsonModel(
            [
                'data' => "Update Contact"
            ]
        );
    }

    /**
     * @see \Core\Service\AbstractService\AbstractService::delete()
     * @param AbstractService\unknown $contactId
     * @return bool|JsonModel
*/
    public function delete($contactId)
    {
        return new JsonModel(
            [
                'data' => "Delete Contact"
            ]
        );
    }

    /**
     * @see \Core\Service\AbstractService\AbstractService::options()
     */
    public function options()
    {
        return new JsonModel(
            [
                'data' => "Methods Allowed"
            ]
        );
    }
}
