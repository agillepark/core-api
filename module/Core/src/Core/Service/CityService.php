<?php
/**
 * Agille Park - Estacionamentos Inteligentes - Core-API
 * 
 * Copyright (C) Incorpore & WeDev Brasil - Todos os direitos reservados
 * Copia não autorizada deste arquivo é estritamente proibido
 * Proprietário e Confidencial
 *
 * @link      https://github.com/incorpore/core-api para o repositório privado
 * @copyright Copyright (c) 2015 Incorpore Inc. (http://incorpore.com.br)
 * @author   Thiago R. Moreira <loganguns@gmail.com>
 */
 
namespace Core\Service;

use Core\Service\AbstractService\AbstractService;

/**
 * City Service
 *
 * @category Core
 * @package  Service
 * @author   Thiago R. Moreira <loganguns@gmail.com>
 */
class CityService extends AbstractService
{

    /**
     *
     */
    public function getList()
    {
        $this->queryBuilder()->select('c')
            ->from('Core\Entity\City', 'c')
            ->orderBy('c.cityId', 'ASC')
            ->setMaxResults(20)
            ->setFirstResult(0);
        /*
        if (! empty($this->params()->fromQuery('enterprise'))) {
            
            $queryBuilder->select('c', 'e')
                ->leftJoin('c.enterprise', 'e')
                ->where('e.enterpriseId = :enterprise')
                ->setParameter(
                    'enterprise',
                    $this->params()->fromQuery('enterprise')
                );
        }
        */
        return $this;
    }

    /**
     * Retorna um inividuo do recurso solicitado
     *
     * @param mixed $cityId
     *
     * @return mixed
     * @internal param mixed $id
     */
    public function get($cityId)
    {
        $this->queryBuilder()->select('c')
            ->where('c.cityId =' . $cityId)
            ->from('Core\Entity\City', 'c');
        // ->leftJoin('u.address', 'a')
        
        
        return $this;
    }

    /**
     * Cria um novo recurso
     *
     * @param  mixed $data
     *
     * @return mixed
     */
    public function create($data)
    {
        return new JsonModel(
            [
                'data' => "Create City"
            ]
        );
    }

    /**
     * Atualiza um recurso exixtente
     *
     * @param mixed  $cityId
     * @param  mixed $data
     * @return mixed
     * @internal param mixed $id
     */
    public function update($cityId, $data)
    {
        $crypt = new CryptResponse();
        
        foreach ($data as $key => $curr) {
            $data[$key] = $crypt->decrypt($curr);
        }
        
        $qb = $this->getEntityManager()->createQueryBuilder();
        $q = $qb->update('Core\Entity\City', 'c')
            ->set('u.docNumber', '?1')
            ->where('u.cityId = ?2')
            ->setParameter(1, $data['docNumber'])
            ->setParameter(2, $cityId)
            ->getQuery();
        $q->execute();
        
        /*
         * $data = new City();
         * $data->setId(123);
         * $data->setName('test');
         *
         * $entityManager->merge($data);
         * $entityManager->flush();
         */
        
        // return $this->serialize(array($data), false);
        return $this->serialize(null);
    }

    /**
     * @param AbstractService\unknown $cityId
     * @return bool
     * @internal param unknown $id
     *
     */
    public function delete($cityId)
    {
        return new JsonModel(
            [
                'data' => "Delete City"
            ]
        );
    }

    /**
     * @return JsonModel
     */
    public function options()
    {
        return new JsonModel(
            [
                'data' => "Methods Allowed"
            ]
        );
    }
}
