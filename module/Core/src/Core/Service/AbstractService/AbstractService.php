<?php
/**
 * Agille Park - Estacionamentos Inteligentes - Core-API
 * Copyright (C) Incorpore & WeDev Brasil - Todos os direitos reservados
 * Copia não autorizada deste arquivo é estritamente proibido
 * Proprietário e Confidencial
 * @link      https://github.com/incorpore/core-api para o repositório privado
 * @copyright Copyright (c) 2015 Incorpore Inc. (http://incorpore.com.br)
 * @author    Thiago R. Moreira <loganguns@gmail.com>
 */

namespace Core\Service\AbstractService;

use Doctrine\ORM\AbstractQuery;
use Doctrine\ORM\EntityManager;
use Zend\Mvc\Controller\Plugin\Params;

/**
 * Abstract Service
 * @category    Core
 * @package     Service
 * @subpackage  AbstractService
 * @author      Thiago R. Moreira <loganguns@gmail.com>
 */
abstract class AbstractService
{
    /**
     * Entity Manager usado para manipular as entidades
     * @var Doctrine\ORM\EntityManager
     */
    protected $entityManager;
    
    /**
     * Doctrine Query Builder
     * @var Doctrine\Query\Builder
     */
    protected $queryBuilder;
    
    /**
     * Paranetros Query Builder
     * @var array
     */
    protected $queryBuilderParams;
    
    /**
     * Manipula uma entidade
     * @return Doctrine\ORM\EntityManager
     */
    public function getEntityManager()
    {
        return $this->entityManager;
    }
    
    /**
     * Contruct
     *
     * @param EntityManager $entityManager
     */
    public function __construct(EntityManager $entityManager, Params $params)
    {
        $this->entityManager = $entityManager;
        $this->queryBuilder = $this->getEntityManager()->createQueryBuilder();
        $this->params = $params;
    }
    
    /**
     *
     */
    public function queryBuilder()
    {
        return $this->queryBuilder;
    }

    /**
     *
     */
    public function params()
    {
        return $this->params;
    }

    /**
     *
     */
    abstract public function getList();
    
    /**
     * Retorna um inividuo do recurso solicitado
     *
     * @param  mixed $id
     *
     * @return mixed
     */
    abstract public function get($id);
    
    /**
     * Cria um novo recurso
     *
     * @param  mixed $data
     *
     * @return mixed
     */
    abstract public function create($data);
    
    /**
     * Atualiza um recurso exixtente
     *
     * @param  mixed $id
     * @param  mixed $data
     *
     * @return mixed
     */
    abstract public function update($id, $data);
    
    /**
     * @param unknown $id
     *
     * @return boolean
     */
    abstract public function delete($id);

    /**
     * @return unknown
     */
    public function getResult()
    {
        $this->queryBuilder()
            ->setMaxResults(20)
            ->setFirstResult(0);
        
        $results = $this->queryBuilder()->getQuery()->getResult(
            AbstractQuery::HYDRATE_ARRAY
        );
        return $results;

    }
}
