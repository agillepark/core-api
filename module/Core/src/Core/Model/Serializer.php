<?php
/**
 * Agille Park - Estacionamentos Inteligentes - Core-API
 * Copyright (C) Incorpore & WeDev Brasil - Todos os direitos reservados
 * Copia não autorizada deste arquivo é estritamente proibido
 * Proprietário e Confidencial
 * @link      https://github.com/incorpore/core-api para o repositório privado
 * @copyright Copyright (c) 2015 Incorpore Inc. (http://incorpore.com.br)
 * @author    Thiago R. Moreira <loganguns@gmail.com>
 */

namespace Core\Model;

use Core\Model\Crypt;
use JMSSerializerModule\View\Serializer as JMSerializer;
use Zend\Config\Config;
use Zend\View\Model\JsonModel;
use Zend\ServiceManager\ServiceManager;

/**
 * Serializa a saida de dados da API
 * @category Core
 * @package  Model
 * @author   Thiago R. Moreira <loganguns@gmail.com>
 */
class Serializer
{

    /**
     * Confifura Criptografia na saida de dados: true|false
     * @var boolean
     */
    protected $security;

    /**
     * Formato de saida da API: json|xml
     *Response
     * @var string
     */
    protected $format;
    
    /**
     * Armazena configuração APP
     * @var \Zend\Config\Config
     */
    protected $config;
    
    /**
     * @var Zend\ServiceManager\ServiceManager
     */
    protected $serviceManager;

    /**
     * Constructor
     *
     * @param ServiceManager $serviceManager
     */
    public function __construct(ServiceManager $serviceManager)
    {
        $this->config = new Config(
            include __DIR__
                . '/../../../../../config/autoload/serializer.local.php'
        );
        $this->format = $this->config->format;
        $this->security = $this->config->security;
        $this->serviceManager = $serviceManager;
    }

    /**
     * Inicia Service Manager
     * @return ServiceManager
     */
    public function serviceManager()
    {
        /*
        if (null === $this->serviceManager) {
            $this->serviceManager = $serviceManager;
        }
        */
        return $this->serviceManager;
    }
    
    /**
     * Formata Array de dados para saida da API
     *
     * @param array $data
     *
     * @return array $stampedArrayResponse
     */
    public function formatArrayResponse($data)
    {
        $crypt = new Crypt();
        $security = $this->security;
        
        $data = function () use ($crypt, $security, $data) {
            if ($security) {
                return $crypt->encrypt(json_encode($data));
            } else {
                return $data;
            }
        };
        
        $formatedArrayResponse = [
            'timestamp' => time(),
            'data' => $data()
        ];

        return $formatedArrayResponse;
    }

    /**
     * Serializa os dados para saida da API
     *
     * @param  array $data
     *
     * @return JsonModel
     * @throws \Exception
     */
    public function serialize($data)
    {

        if ($this->format == 'json') {

            //if ($data != false) {

            /* if ($this->security) { */

            $output = $this->formatArrayResponse($data);

            /* } else {

                $serialize = $this->serviceManager()->get(
                    'jms_serializer.serialize'
                );
                $serializedData = $serialize->serialize(array($data), 'json');
                $output = json_decode($serializedData);

            } */

            //} else {

            //$this->getResponse()->setStatusCode(404);

            //}
            
        } else {
            
            throw new \Exception("Not yet implemented");
        }

        return new JsonModel($output);
    }
}
