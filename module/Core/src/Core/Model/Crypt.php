<?php
/**
 * Agille Park - Estacionamentos Inteligentes - Core-API
 * Copyright (C) Incorpore & WeDev Brasil - Todos os direitos reservados
 * Copia não autorizada deste arquivo é estritamente proibido
 * Proprietário e Confidencial
 * @link      https://github.com/incorpore/core-api para o repositório privado
 * @copyright Copyright (c) 2015 Incorpore Inc. (http://incorpore.com.br)
 * @author    Thiago R. Moreira <loganguns@gmail.com>
 */

namespace Core\Model;

use Zend\Crypt\Key\Derivation\Pbkdf2;
use Zend\Math\Rand;
use Zend\Crypt\BlockCipher;
use Zend\Config\Config;

/**
 * Manipula a criptografia de dado da API
 * @category Core
 * @package  Model
 * @author   Thiago R. Moreira <loganguns@gmail.com>
 */
class Crypt
{

    /**
     * @var ServiceManager
     */
    protected $serviceManager;
    
    /**
     * @var array
     */
    protected $algo;
    
    /**
     * @var ServiceManager
     */
    protected $key;
    
    /**
     * @var \Zend\Config\Config
     */
    protected $config;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->config = new Config(
            include __DIR__ . '/../../../../../config/autoload/crypt.local.php'
        );
        $this->key = hex2bin($this->config->defaultKey);
        $this->algo = $this->config->algo;
    }

    /**
     * Service Manager
     *
     * @param ServiceManager $serviceManager
     *
     * @return ServiceManager
     */
    public function setServiceManager(ServiceManager $serviceManager)
    {
        $this->serviceManager = $serviceManager;
        
        return $this->serviceManager;
    }

    /**
     * Gerador de Key Randomico
     * (Não utilizado, usamos uma chave fixa)
     *
     * @param string $pass
     *
     * @return ambigous <string, \Zend\Crypt\Key\Derivation\string>
     */
    public function keygen($pass = null)
    {
        $uniqueRandonStr = function ($lenght) {
            $bytes = openssl_random_pseudo_bytes($lenght);
            return bin2hex($bytes);
        };
        
        if ($pass == null) {
            $pass = $uniqueRandonStr(32);
        }
        
        $salt = Rand::getBytes(strlen($pass), true);
        $this->key = Pbkdf2::calc('sha256', $pass, $salt, 10000, strlen($pass) * 2);
        
        return $this->key;
    }

    /**
     * Criptografa uma mensagem
     *
     * @param string $msg
     *
     * @return \Zend\Crypt\string
     */
    public function encrypt($msg)
    {
        $blockCipher = BlockCipher::factory('mcrypt', $this->algo);
        $blockCipher->setKey($this->key);
        $result = $blockCipher->encrypt($msg);

        return $result;
    }

    /**
     * Decriptografa uma mensagem
     *
     * @param string $msg
     *
     * @return Ambigous <\Zend\Crypt\string, \Zend\Crypt\bool>
     */
    public function decrypt($msg)
    {
        $blockCipher = BlockCipher::factory('mcrypt', $this->algo);
        $blockCipher->setKey($this->key);
        $result = $blockCipher->decrypt($msg);
        
        return $result;
    }
}
