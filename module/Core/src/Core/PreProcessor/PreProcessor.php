<?php
/**
 * Agille Park - Estacionamentos Inteligentes - Core-API
 * Copyright (C) Incorpore & WeDev Brasil - Todos os direitos reservados
 * Copia não autorizada deste arquivo é estritamente proibido
 * Proprietário e Confidencial
 * @link      https://github.com/incorpore/core-api para o repositório privado
 * @copyright Copyright (c) 2015 Incorpore Inc. (http://incorpore.com.br)
 * @author    Thiago R. Moreira <loganguns@gmail.com>
 */

namespace Core\PreProcessor;

use Zend\Mvc\MvcEvent;
use Core\Service\Auth;
use Zend\ServiceManager\ServiceManager;

/**
 * Class responsable for pre-processing Api requisitions
 * @category Core
 * @package  PreProcessor
 * @author   Thiago R. Moreira <loganguns@gmail.com>
 */
class PreProcessor
{
    protected $e;

    /**
     * @param MvcEvent $e
     */
    public function __construct(MvcEvent $e)
    {
        $this->e = $e;
        $this->cacheClient();
    }

    /**
     * Executed in pre-processing, before any action.
     * Verifies the user's resource access permission
     * @return null|\Zend\Http\PhpEnvironment\Response
     * @throws \Exception
     * @internal param MvcEvent $e
     */
    public function validate()
    {
        //$this->configureEnvironment($this->e);
        $auth = new Auth();
        $routeMatch = $this->e->getRouteMatch();
        $routeName = $routeMatch->getMatchedRouteName();
        $module = $routeMatch->getParam('module', false);
        $request = $routeMatch->getParam('entity', false);
        /*
        $request = $this->e->getParam('service', false);
        var_dump($request);
        */


        //$cache = $this->e->getApplication()->getServiceManager()->get
        //('Cache');
        //$cached = $cache->getItem('api');
        /*
        if ($cached) {
            $token = $this->e->getRequest()->getHeaders('Authorization');
            if (!$token) {
                throw new \Exception("Token required");
            }
            $this->checkAuthorization($auth, $token, $module . '.' . $request);
            return true;
        }*/
        /** @noinspection PhpUnnecessaryFullyQualifiedNameInspection */
        $apiConfig = new \Zend\Config\Config(
            include __DIR__ . '/../../../../../config/autoload/api.local.php'
        );


        //acesso requer um token válido e permissões de acesso
        if ($apiConfig['authorization'] == 1) {
            $authHeader = $this->e->getRequest()->getHeaders('Auth');
            $credentialHeader = $this->e->getRequest()->getHeaders
            ('Credential');

            if (empty($authHeader)) {
                throw new \Exception("Auth token required");
            }
            if (empty($credentialHeader)) {
                throw new \Exception("credential token required");
            }
            //var_dump($request);
            $this->checkAuthorization($authHeader, $credentialHeader, $request);
        }
        return true;
    }

    /**
     * Performs the authorization test
     *
     * @param  Auth   $auth    Auth service
     * @param  Header $token   Requisition token
     * @param  string $request Requested service
     *
     * @return boolean
     */
    private function checkAuthorization($auth, $token, $request)
    {
        /*
        switch ($auth->authorize($parameters)) {
            case Auth::INVALID:
                throw new \Exception("Invalid token");
                break;
            case Auth::EXPIRED:
                throw new \Exception("Expired token");
                break;
            case Auth::DENIED:
                throw new \Exception("Denied access");
                break;
        }
        */
        return true;
    }

    /**
     * Verifies if the API is being accessed by a tests environment,
     * and configures the environment accordingly
     * @internal param MvcEvent $e Evento
     */
    private function configureEnvironment()
    {
        if (!method_exists($this->e->getRequest(), 'getHeaders')) {
            return;
        }
        $env = $this->e->getRequest()->getHeaders('Environment');
        if ($env) {
            switch ($env->getFieldValue()) {
                case 'testing':
                    putenv("ENV=testing");
                    break;
                case 'jenkins':
                    putenv("ENV=jenkins");
                    break;
            }
        }
        return;
    }

    /**
     * Inicia Service Manager
     * @return ServiceManager
     */
    public function getServiceLocator()
    {
        /*
        if (null === $this->serviceManager) {
            $this->serviceManager = $serviceManager;
        }
        */
        return $this->e->getApplication()->getServiceManager();
    }

    /**
     * @return bool
     */
    private function cacheClient()
    {
        //$aws    = $this->getServiceLocator()->get('aws');
        //$client = $aws->get('ElastiCache');
        //return $client;

        return true;
    }
}