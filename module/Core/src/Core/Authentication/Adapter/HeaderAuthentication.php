<?php
/**
 * Agille Park - Estacionamentos Inteligentes - Core-API
 * Copyright (C) Incorpore & WeDev Brasil - Todos os direitos reservados
 * Copia não autorizada deste arquivo é estritamente proibido
 * Proprietário e Confidencial
 * @link      https://github.com/incorpore/core-api para o repositório privado
 * @copyright Copyright (c) 2015 Incorpore Inc. (http://incorpore.com.br)
 * @author    Thiago R. Moreira <loganguns@gmail.com>
 */

namespace Core\Authentication\Adapter;

use Zend\Authentication\Adapter\AdapterInterface;
use Zend\Authentication\Result;
use Zend\Http\Request;

/**
 * Restful Controller seleciona o Entity Service solicitado
 * @category    Core
 * @package     Authentication
 * @subpackage  Adapter
 * @author      Thiago R. Moreira <loganguns@gmail.com>
 */
class HeaderAuthentication implements AdapterInterface
{
    protected $request;

    /**
     * @param $request
     */
    public function __construct($request)
    {
        $this->request = $request;
    }

    /**
     * @return Result
     */
    public function authenticate()
    {
        $request = $this->request;
        $headers = $request->getHeaders();

        // Check Authorization header presence
        if (!$headers->has('Authorization')) {
            return new Result(
                Result::FAILURE,
                null,
                ['Authorization header missing']
            );
        }

        // Check Authorization prefix
        $authorization = $headers->get('Authorization')
            ->getFieldValue();
        if (strpos($authorization, 'PRE') !== 0) {
            return new Result(Result::FAILURE, null, [
                'Missing PRE prefix'
            ]);
        }

        // Validate public key
        $publicKey = $this->extractPublicKey($authorization);
        $user = $this->getUserRepository()
            ->findByPublicKey($publicKey);
        if (null === $user) {
            $code = Result::FAILURE_IDENTITY_NOT_FOUND;
            return new Result($code, null, [
                'User not found based on public key'
            ]);
        }

        // Validate signature
        $signature = $this->extractSignature($authorization);
        $hmac = $this->getHmac($request, $user);
        if ($signature !== $hmac) {
            $code = Result::FAILURE_CREDENTIAL_INVALID;
            return new Result($code, null, [
                'Signature does not match'
            ]);
        }

        return new Result(Result::SUCCESS, $user);
    }
}