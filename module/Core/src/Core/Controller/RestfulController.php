<?php
/**
 * Agille Park - Estacionamentos Inteligentes - Core-API
 * Copyright (C) Incorpore & WeDev Brasil - Todos os direitos reservados
 * Copia não autorizada deste arquivo é estritamente proibido
 * Proprietário e Confidencial
 * @link      https://github.com/incorpore/core-api para o repositório privado
 * @copyright Copyright (c) 2015 Incorpore Inc. (http://incorpore.com.br)
 * @author    Thiago R. Moreira <loganguns@gmail.com>
 */

namespace Core\Controller;

use Zend\Mvc\Controller\AbstractRestfulController;
use Zend\View\Model\JsonModel;
use Zend\Http\Response;
use Zend\Mvc\MvcEvent;
use Zend\View\Model\ViewModel;
use Core\Model\Serializer;
use Core\Service\AbstractService\AbstractService;

/**
 * Restful Controller seleciona o Entity Service solicitado
 * @category Core
 * @package  Controller
 * @author   Thiago R. Moreira <loganguns@gmail.com>
 */
class RestfulController extends AbstractRestfulController
{

    /**
     * Entity Manager usado para manipular as entidades
     * @var Doctrine\ORM\EntityManager
     */
    protected $entityManager;
    
    /**
     * Nome do serviço requisitado
     * @var string
     */
    protected $serviceName;

    protected $eventIdentifier = 'Core\Controller';

    /**
     *
     */
    public function __construct()
    {
        
    }
    
    /**
     * @param MvcEvent $e
     *
     * @return mixed|void
     */
    public function onDispatch(MvcEvent $e)
    {
        parent::onDispatch($e);
    }
    
    /**
     * Manipula uma entidade
     * @return Doctrine\ORM\EntityManager
     */
    public function getEntityManager()
    {
        if (null === $this->entityManager) {
            $this->entityManager = $this->getServiceLocator()->get(
                'doctrine.entitymanager.orm_default'
            );
        }
        return $this->entityManager;
    }

    /**
     * Inincia Entity Manager
     *
     * @param $entityManager
     */
    public function setEntityManager($entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * Inincia o Service Resource para providenciar o recurso solicitado
     * @return Core\Service
     * @throws \Exception
     */
    public function serviceResource()
    {
        /* Refatorrar */
        $serviceName = $this->params('service');
        
        if ($serviceName == null) {
            $serviceName = 'index';
        }
        /* */
        
        $serviceClass = '\\Core\\Service\\' . ucfirst($serviceName) . 'Service';
        
        if (class_exists($serviceClass)) {
            
            $serviceManager = new $serviceClass($this->getEntityManager(), $this->params());
            //$serviceManager = new UserService($this->getEntityManager());
            
            if ($serviceManager instanceof AbstractService) {
                
                //$serviceManager = $serviceManager->getResult();
                return $serviceManager;
                
            } else {
                
                //throw new \Exception('Not AbstractService');
                //return false;
                return $serviceManager;
            }
            
        } else {
            
            throw new \Exception('Service not defined');
            
        }
    }
    
    /**
     * Retorna uma lista do recurso solicitado
     * @return mixed
     */
    public function getList()
    {
        $service = $this->serviceResource()->getList()->getResult();
        
        return $this->serializer($service);
    }
    
    /**
     * Retorna um inividuo do recurso solicitado
     *
     * @param mixed $id
     *
     * @return mixed
     */
    public function get($id)
    {
        $data = $this->serviceResource()->get($id)->getResult();
        
        return $this->serializer($data);
    }
    
    /**
     * Cria um novo recurso
     *
     * @param  mixed $data
     *
     * @return mixed
     */
    public function create($data)
    {
        return $this->serializer(
            $this->serviceResource()->create($data)
        );
    }
    
    /**
     * Atualiza um recurso exixtente
     *
     * @param mixed $id
     * @param mixed $data
     *
     * @return mixed
     */
    public function update($id, $data)
    {
        return new JsonModel(
            [
                'data' => "Update Resource $id, $data"
            ]
        );
    }
    
    /**
     * Deleta um recurso existente
     *
     * @param mixed $id
     *
     * @return mixed
     */
    public function delete($id)
    {
        return new JsonModel(
            [
                'data' => "Delete Resource $id"
            ]
        );
    }

    /**
     * @param array $data
     *
     * @return \Zend\View\Model\JsonModel
     */
    public function serializer($data)
    {
        $serializer = new Serializer($this->getServiceLocator());
        return $serializer->serialize($data);
    }
}
