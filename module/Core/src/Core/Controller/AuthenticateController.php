<?php
/**
 * Agille Park - Estacionamentos Inteligentes - Core-API
 * Copyright (C) Incorpore & WeDev Brasil - Todos os direitos reservados
 * Copia não autorizada deste arquivo é estritamente proibido
 * Proprietário e Confidencial

 * @link      https://github.com/incorpore/core-api para o repositório privado
 * @copyright Copyright (c) 2015 Incorpore Inc. (http://incorpore.com.br)
 * @author    Thiago R. Moreira <loganguns@gmail.com>
 */

namespace Core\Controller;

use Zend\Mvc\Controller\AbstractRestfulController;
use Zend\Http\Response;
use Core\Model\Serializer;

/**
 * Restful Controller seleciona o Entity Service solicitado
 *
 * @category Core
 * @package  Controller
 * @author   Thiago R. Moreira <loganguns@gmail.com>
 */
class AuthenticateController extends AbstractRestfulController
{
    /**
     * Retorna uma lista do recurso solicitado
     * @return mixed
     */
    public function getList()
    {
        //$serviceManager = $this->getServiceLocator();

        return $this->serializer(['token']);
    }

    /**
     * @param array $data
     *
     * @return \Zend\View\Model\JsonModel
     */
    public function serializer($data)
    {
        $serializer = new Serializer($this->getServiceLocator());
        return $serializer->serialize($data);
    }
}
